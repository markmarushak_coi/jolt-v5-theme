<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
<script type="text/javascript">
	
	

	
	$(function(){

	// clickdown
		$('a.clickdown').each(function() {
			var $this = $(this);
			var index = $this.attr('id').match(/(\d+)$/, '');
			var number = parseInt(index[1]);
			$this.click(function() {
				$('#slickbox' + number).slideToggle(400);
				$this.toggleClass('opened').blur();
				return false;
			});
		});
	// end clickdown

	//$(document).ready(function(){
		//console.log('WHy is that');
		//alert("Why is that ?");
		$('#domain-search-form').bind('submit' , function(e){
			e.preventDefault();
			var error = false;
			$("#slickbox1").hide();
			$(".error").remove();
			//Validate the Domain First
			if($("input[name=domainname]").val() == '') {
				$(".domains-form").after('<div class="error">Please Enter the Domain name of your choice.</div>');
				$("input[name=domainname]").focus();
				error = true;
			}
			
			if(fncheckAllowedDomainName($("input[name=domainname]").val())) {
				$(".domains-form").after('<div class="error">Please Enter Valid Domainname.</div>');
				$("input[name=domainname]").focus();
				error = true;
			}
			//if(fncheckAllowedDomainName())
			if(!error) {
				$("#domains-table-container").remove();
				$('.domains-top').removeAttr("style");
				$.ajax({
						url: '/domain-search.php',
						type: 'post',
						data: $(this).serialize(),
						async: false,
						beforeSend: function() { 
							//$obj.mJqueryLoader({status:true});
							//$obj.mJqueryProgress({status:true}); 
						},
						cache: true,
						dataType: 'json',
						crossDomain: true,
						success: function(data) {
							var domainname = $("input[name=domainname]").val();
							var content_string = '';
							content_string += '<div class="clear"></div><div id="domains-table-container">';
								content_string += '<form id="domains-form-submit" method="post" action="https://my.webhostingbuzz.com/cart.php?a=add&amp;domain=register">';
									content_string += '<table class="domain-collection zebra-striped">';
									content_string += '<thead><tr><th></th><th>Domain Name</th><th class="textcenter">Status</th><th class="textcenter">More Info</th></tr></thead><tbody>	';
									$.each(data[domainname], function(i, item) {
										//content_string += '<tr><td>'+domainname+i+'</td><td>'+data[domainname][i]['whoinfo']['status']+'</td><td>'++'</td></tr>';
										if(data[domainname][i]['whoinfo']['status'] == 'available') {
											content_string += '<tr><td class="textcenter"><input name="domains[]" value="'+domainname+i+'" type="checkbox" class="domains-checked" /></td><td>'+domainname+i+'</td><td class="textcenter domcheckersuccess">'+data[domainname][i]['whoinfo']['status'].toUpperCase()+'</td><td class="textcenter">'+domains_dropdown(data[domainname][i]['pricing']['pricing']['domainregister']['USD'] , i , domainname)+'</td></tr>';
										} else {
											content_string += '<tr><td class="textcenter"><input name="domains[]" value="'+domainname+i+'" type="checkbox" class="domains-checked" /></td><td>'+domainname+i+'</td><td class="textcenter domcheckererror">'+data[domainname][i]['whoinfo']['status'].toUpperCase()+'</td><td class="textcenter">'+domains_dropdown(data[domainname][i]['pricing']['pricing']['domainregister']['USD'] , i , domainname)+'</td></tr>';
										}
									});
										content_string += '<tr><td colspan="4" style="border:none;"><p align="center"><input type="submit" value="Order Now &raquo;" class="btn error"></p></td></tr>';
									
									content_string += '</tbody></table>';
								content_string += ' </form>';
							content_string += ' </div>';
							/*$('.domain-collection').slideDown('slow' , function(e){
							});*/
							$('.domains-form-box').after(content_string);
							//Add Domain Top Height
							//console.log(data[domainname]);
							//Check Height of domains-top then add the height
							var current_height = parseInt($('.domains-top').css("height"));
							var domains_table_height = $('#domains-table-container').height();
							
							var new_height = (current_height + domains_table_height) + 20;
							
							$('.domains-top').animate({height:new_height},200);
							
							//$('.domains-top').css('height' , '573px');
							
							$('#domains-form-submit').bind('submit' , function(e){
								
								e.preventDefault();
								
								
								 var chkArray = [];
     
								/* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
								$(".domains-checked:checked").each(function() {
									chkArray.push($(this).val());
								});
								 
								/* we join the array separated by the comma */
								var selected;
								selected = chkArray.join(',') + ",";
								 
								/* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
								if(selected.length > 1){
									$(this).unbind('submit').submit();
								} else {
									//$("#domains-table-container").after('<div class="error">Please Select Atleast one domainname.</div>');
									alert('Please Select At least one domain name.');
									//$("input[name=domainname]").focus();
									//error = true;   
								}
								
							});
							
						}
				 });
			}

		});
		
		
		$.ajax({
					url: '/whmcs-api.php',
					type: 'post',
					data: ({ action : 'getdomainpricing'}),
					async: false,
					beforeSend: function() { 
						//$obj.mJqueryLoader({status:true});
						//$obj.mJqueryProgress({status:true}); 
						$("#slickbox1").html('<em>Loading....</em>');
					},
					cache: true,
					dataType: 'json',
					crossDomain: true,
					success: function(data) {
						if(data.whmcsapi.result == 'success') {
							var slickbox1_html = '';
							var cnt = 1;
							$.each(data.whmcsapi.domains.domain, function(i, item) {
								if(cnt == 1) {
									slickbox1_html += '<div class="domains-check-item"><input name="tld[]" type="checkbox" class="checkbox" checked="checked" value="'+data.whmcsapi.domains.domain[i].extension+'" /><label>'+data.whmcsapi.domains.domain[i].extension+'</label></div>';
								} else {
									slickbox1_html += '<div class="domains-check-item"><input name="tld[]" type="checkbox" class="checkbox" value="'+data.whmcsapi.domains.domain[i].extension+'" /><label>'+data.whmcsapi.domains.domain[i].extension+'</label></div>';
								}
								cnt++;
							});
							$("#slickbox1").html(slickbox1_html);
							$("#slick-toggle1").html(data.whmcsapi.domains.domain[0].extension);
						}
					}
			 });
		//slickbox1
	
	//});
	});
	
	
		fncheckAllowedDomainName = function(name) {
			var regEx = /([A-Za-z0-9\s\-])/;
			for (var i = 0; i < name.length; i++) {
				if (!regEx.test(name.charAt(i))) {
				  error = true;
				  return true;
				  break;
				}
			}		
			return false;
		}


		domains_dropdown = function(domain_pricing , domain_tld , domainname ) {
			
			var content_string_2 = '';
			
			content_string_2 += '<select name="domainsregperiod['+ domainname+domain_tld +']">';
			$.each(domain_pricing, function(i, item) {
				if(item > 0) {
					content_string_2 += '<option value="'+reverse_domain_regperiod(i)+'">' + domain_regperiod_year(i) + ' $' + item + ' USD' + '</option>';
				}
			});
			content_string_2 += '</select>';
			
			return content_string_2;
		}
	
	
		domain_regperiod_year = function(billing_cycle) {
		
			switch(billing_cycle) {
			
				case 'anually':
					return '1 Year/s @ '; 
				break;
				
				
				case 'bianually':
					return '2 Year/s @ ';
				break;


				case 'trianually':
					return '3 Year/s @ ';
				break;

				case 'quadanually':
					return '4 Year/s @ ';
				break;
			}
		}
		
		reverse_domain_regperiod = function(billing_cycle) {

			switch(billing_cycle) {
			
				case 'anually':
					return '1'; 
				break;
				
				
				case 'bianually':
					return '2';
				break;


				case 'trianually':
					return '3';
				break;

				case 'quadanually':
					return '4';
				break;
			}
		}
	
</script>
<style>

	.display-none {
		display:none;
	}
	.error {
		color:#990000;
		padding:2px;
		margin:10px 0;
	}


	.domain-collection {

		width: 650px;
		margin-bottom: 18px;
		padding: 0;
		border-collapse: separate;
		font-size: 13px;
		border: 1px solid #ddd;
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		border-spacing: 0;
		

	}

	thead {
		display: table-header-group;
		vertical-align: middle;
		border-color: inherit;
	}
	
	tr {
		display: table-row;
		vertical-align: inherit;
		border-color: inherit;
	}
	table th {
		padding-top: 9px;
		font-weight: bold;
		vertical-align: middle;
	}
	
	table th, table td {
		padding: 10px 10px 9px;
		line-height: 18px;
		text-align: left;
	}	
	
	.textcenter {
		text-align: center;
	}
	
	
	tbody {
		display: table-row-group;
		vertical-align: middle;
		border-color: inherit;
	}


	input[type=checkbox] {
		width: auto;
		height: auto;
		padding: 0;
		margin: 3px 0;
		line-height: normal;
		border: none;
		cursor: pointer;
		transition: border linear 0.2s, box-shadow linear 0.2s;
		box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.1);
		border-radius: 3px;
		font-family: "Open Sans", sans-serif;
		-webkit-appearance: checkbox;
		box-sizing: border-box;
		background-color: initial;
		-webkit-rtl-ordering: logical;
		-webkit-user-select: text;
		letter-spacing: normal;
		word-spacing: normal;
		text-transform: none;
		text-indent: 0px;
		text-shadow: none;
		text-align: start;
		-webkit-writing-mode: horizontal-tb;
	}
	
	table td {
		border-top: 1px solid #ddd;
		vertical-align: top;
	}
	
	
	
	
	
	select {
		
		height: 27px;
		line-height: 27px;
		border: 1px solid #ccc;
		border-radius: 3px;
		color: #808080;
		display: inline-block;
		font-size: 13px;
		line-height: 18px;
		padding: 4px;
		width: 150px;
	}

	 .domcheckersuccess {
		color: #009933;
	 }
	 
	 .domcheckererror {
		color: #cc0000;
	 }
	
	.btn.danger, .alert-message.danger, .btn.error, .alert-message.error {
		background-color: #c43c35;
		background-image: -moz-linear-gradient(center top , #ee5f5b, #c43c35);
		background-repeat: repeat-x;
		border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
		text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
		color: #ffffff;
		height: auto;
		width: auto;
		cursor: pointer;
		-moz-border-bottom-colors: none;
		-moz-border-left-colors: none;
		-moz-border-right-colors: none;
		-moz-border-top-colors: none;
		background-color: #e6e6e6;
		background-repeat: no-repeat;
		border-color: #ccc #ccc #bbb;
		border-image: none;
		border-radius: 4px;
		border-style: solid;
		border-width: 1px;
		box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.05);
		cursor: pointer;
		display: inline-block;
		font-size: 13px;
		line-height: normal;
		padding: 5px 14px 6px;
		transition: all 0.1s linear 0s;
		background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ee5f5b), color-stop(100%, #c43c35));
	} 
	
	a.domains-inp-sel, a.domains-inp-sel:hover {
		/*background-clip: padding-box;
		background-image: -moz-linear-gradient(center bottom , #ededed 0%, #fff 69.56%, #fff 100%);
		border: 1px solid #d9d9d9;*/
		border: 1px solid #d9d9d9;
		/*border-radius: 2px;*/
		/*border-left:none;*/
		color: #000;
		font-size: 15px;
		height: 42px;
		width:10%;
		/*line-height: 25px;*/
		/*min-width: 33px;*/
		padding: 9px 5px;
		text-decoration: none;
		float:left;
		/*box-shadow: 0 0 5px #ccc;*/
	}
 	.domains-inp {
		height: 42px;
		width:90% !important;
	}
	.domains-form {
		box-shadow: 0 0 5px #ccc;
		padding:0px !important;
		width:100%;
	}
	.domains-form-box .slickbox{width: 90%;float: left;border-radius: 2px;padding:10px;border:1px solid #ccc;margin:10px 0 0;overflow: hidden;}
	.domains-form-box .slickbox .domains-check-item{width: 33.3334%;float: left;margin:0 0 3px;}
	.domain_search .domains-form-box .domains-form input.domains-inp{width: 90% !important; border-radius:none !important;box-shadow:none !important;padding:10px !important; border-right:none !important;}

</style>
<div class="domains-form-box">
<form name="domain-search" action="javascript:void(0);" id="domain-search-form" method="post">
	<input type="hidden" name="action" value="search" />
    <div class="domains-form">
		<input type="text" name="domainname" value="" placeholder="your-domain" class="domains-inp" />
		<!-- First TLD from the list -->
        <?php /*?><p class="clickdown domains-inp-sel" style="border: solid 1px #000000;" id="slick-toggle1"><!--.org.uk<span>&nbsp;</span>--></p><?php */?>
        <a class="clickdown domains-inp-sel" target="_blank" href="#" id="slick-toggle1"><!--.org.uk<span>&nbsp;</span>--></a>
		<div class="clear"></div>
        <!-- First TLD from the list -->
		
	</div>
    <input type="submit" class="domains-submit" value="search" />
    <div id="slickbox1" class="slickbox display-none"></div>
	
	<div class="clear"></div>
</form>
</div>