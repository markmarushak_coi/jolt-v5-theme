<?php
/**
 * jolt-v5 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package jolt-v5
 */

if (!defined('_S_VERSION')) {
    // Replace the version number of the theme on each release.
    define('_S_VERSION', '1.0.0');
}



if (!function_exists('jolt_v5_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function jolt_v5_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on jolt-v5, use a find and replace
         * to change 'jolt-v5' to the name of your theme in all the template files.
         */
        load_theme_textdomain('jolt-v5', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(
            array(
                'menu-1' => esc_html__('Primary', 'jolt-v5'),
            )
        );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support(
            'html5',
            array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
                'style',
                'script',
            )
        );

        // Set up the WordPress core custom background feature.
        add_theme_support(
            'custom-background',
            apply_filters(
                'jolt_v5_custom_background_args',
                array(
                    'default-color' => 'ffffff',
                    'default-image' => '',
                )
            )
        );

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support(
            'custom-logo',
            array(
                'height' => 250,
                'width' => 250,
                'flex-width' => true,
                'flex-height' => true,
            )
        );
    }
endif;
add_action('after_setup_theme', 'jolt_v5_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function jolt_v5_content_width()
{
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('jolt_v5_content_width', 640);
}

add_action('after_setup_theme', 'jolt_v5_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function jolt_v5_widgets_init()
{
    register_sidebar(
        array(
            'name' => esc_html__('Sidebar', 'jolt-v5'),
            'id' => 'sidebar-1',
            'description' => esc_html__('Add widgets here.', 'jolt-v5'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        )
    );
}

add_action('widgets_init', 'jolt_v5_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function jolt_v5_scripts()
{
    wp_enqueue_style('jolt-v5-style', get_template_directory_uri() . '/assets/css/main.css', array(), _S_VERSION);
    wp_style_add_data('jolt-v5-style', 'rtl', 'replace');

    require get_template_directory() . '/inc/scripts/loader-js-files.php';

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}
add_action('wp_enqueue_scripts', 'jolt_v5_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
    require get_template_directory() . '/inc/jetpack.php';
}

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


/**
 * Custom Top menu for this theme
 */
require get_template_directory() . '/inc/custom-top-menu.php';
require get_template_directory() . '/inc/custom-footer-menu.php';
require get_template_directory() . '/inc/product-sales-taxonomy.php';
//require get_template_directory() . '/inc/faqs-taxonomy.php';

register_nav_menus([
    'top_menu' => __('Top Menu'),
    'footer_menu' => __('Footer Menu'),
]);

/**
 * Composer Autoload
 */
require get_template_directory() . '/vendor/autoload.php';

/**
 * Awin.
 * Save affiliate click in cookie
 */
function setAwc() {
    if (!empty($_GET['awc'])) {
        setcookie("awc",$_GET['awc'],time()+ 60 * 60 * 24 * 365,"/", ".jolt.co.uk", true, true);
    }
}
add_action('init', 'setAwc');

add_action('after_setup_theme', function () {
    add_theme_support('amp');
});

add_action('init', function () {
    $birthDate = "12/1/2005";

    //explode the date to get month, day and year
    $birthDate = explode("/", $birthDate);

    //get age from date or birthdate
    $company_years = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
        ? ((date("Y") - $birthDate[2]) - 1)
        : (date("Y") - $birthDate[2]));


    if (!defined('COMPANY_YEARS')){
        define('COMPANY_YEARS', $company_years);
    }
}, 1);