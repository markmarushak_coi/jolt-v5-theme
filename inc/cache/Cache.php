<?php

namespace inc\cache;

class Cache
{
    private static $cache;

    private $base_folder;

    public static function instance(): Cache
    {
        if (empty(self::$cache)){
            $inst = new Cache();
            self::$cache = $inst;
        }

        return self::$cache;
    }

    private function __construct(){
        $uploads_dir = wp_get_upload_dir();
        $uploads_dir_basedir = $uploads_dir["basedir"];

        $this->base_folder = $uploads_dir_basedir . DIRECTORY_SEPARATOR . "pages-cache";

        if (false === is_dir($this->base_folder)) {
            $this->MakeBaseFolder();
        }
    }

    private function MakeBaseFolder()
    {
        mkdir($this->base_folder, 0777, true);
    }

    private function MakeFileName($page): string
    {
        return $this->base_folder . DIRECTORY_SEPARATOR . "$page";
    }

    /**
     * This function get page as page_id hash + hash of array
     * For instance:
     * page_id = 132
     * array = result of acf "sections[0=>array[1],1=>array[1]....n=>array[1]]
     * Well, you should make md5 of page_id and array and then if both of this is equal you can open file with content
     * @param $page
     * @return string|null
     */
    public function Find($page): bool
    {
        return is_file($this->MakeFileName($page));
    }

    /**
     * @param $page
     * @param $content
     * @return bool
     */
    public function Store($page, $content): bool
    {
        return file_put_contents($this->MakeFileName($page), $content);
    }

    public function Read($page): string
    {
        return file_get_contents($this->MakeFileName($page));
    }
}