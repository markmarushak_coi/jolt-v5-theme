<?php

$array = [
    'custom' => 'assets/js/custom.js',
    'top-menu' => 'assets/js/top-menu.js',
    'review-slider' => 'assets/js/review-slider/main.js',
    'products-script' => 'assets/js/product.js'
];

foreach ($array as $handle => $src){
    $handle = 'jolt-v5-' . $handle;
    $src = get_template_directory_uri() . '/' . $src;

    wp_enqueue_script($handle, $src, array());
}