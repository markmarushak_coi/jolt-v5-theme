<?php

add_action('init', 'faqs_taxonomy');
function faqs_taxonomy()
{

    $labels = [
        'name' => 'FAQs',
        'singular_name' => 'FAQ',
        'search_items' => 'Search FAQ',
        'all_items' => 'All FAQ',
        'view_item ' => 'View FAQ',
        'parent_item' => 'Parent FAQ',
        'parent_item_colon' => 'Parent FAQ:',
        'edit_item' => 'Edit FAQ',
        'update_item' => 'Update FAQ',
        'add_new_item' => 'Add New FAQ',
        'new_item_name' => 'New FAQ Name',
        'menu_name' => 'FAQ',
    ];

    // список параметров: wp-kama.ru/function/get_taxonomy_labels
    register_taxonomy('FAQs', ['FAQs'], [
        'labels' => $labels,
        'description' => '', // описание таксономии
        'public' => true,
        'hierarchical' => false,
        'rewrite' => true,
        //'query_var'             => $taxonomy, // название параметра запроса
        'capabilities' => array(),
        'show_admin_column' => true, // авто-создание колонки таксы в таблице ассоциированного типа записи. (с версии 3.5)
        'show_ui' => true,
    ]);

}

add_action('admin_menu', 'FAQs_product_add_admin_menus', 1);
function FAQs_product_add_admin_menus()
{
    $url = 'edit-tags.php?taxonomy=FAQs';

    # Add custom admin menu
    add_menu_page('', 'FAQs', 'manage_categories', $url, '', '', 5);
}