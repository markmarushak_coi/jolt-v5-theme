<?php

add_action('init', 'create_sale_product');
function create_sale_product()
{

    $labels = [
        'name' => 'Sales Product',
        'singular_name' => 'Sale Product',
        'search_items' => 'Search Sale',
        'all_items' => 'All Sale',
        'view_item ' => 'View Sale',
        'parent_item' => 'Parent Sale',
        'parent_item_colon' => 'Parent Sale:',
        'edit_item' => 'Edit Sale',
        'update_item' => 'Update Sale',
        'add_new_item' => 'Add New Sale',
        'new_item_name' => 'New Sale Name',
        'menu_name' => 'Sale',
    ];

    // список параметров: wp-kama.ru/function/get_taxonomy_labels
    register_taxonomy('sales', ['sales'], [
        'labels' => $labels,
        'description' => '', // описание таксономии
        'public' => true,
        'hierarchical' => false,
        'rewrite' => true,
        //'query_var'             => $taxonomy, // название параметра запроса
        'capabilities' => array(),
        'show_admin_column' => true, // авто-создание колонки таксы в таблице ассоциированного типа записи. (с версии 3.5)
        'show_ui' => true,
    ]);

}

add_action('admin_menu', 'sales_product_add_admin_menus', 1);
function sales_product_add_admin_menus()
{
    $url = 'edit-tags.php?taxonomy=sales';

    # Add custom admin menu
    add_menu_page('', 'Product Sales', 'manage_categories', $url, '', '', 5);
}