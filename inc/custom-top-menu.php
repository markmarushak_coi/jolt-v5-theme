<?php

/**
 * The function return top menu
 */
function get_top_menu(){

    $menu_name = 'top_menu';
    $locations = get_nav_menu_locations();

    if( $locations && isset( $locations[ $menu_name ] ) ){

        // получаем элементы меню
        $menu_items = wp_get_nav_menu_items( $locations[ $menu_name ]);

        return new ConstructorTopMenu($menu_items);

    }

    return null;

}

class ConstructorTopMenu
{
    private $top = '';

    private $sub_menu = '';

    /**
     * list items in the submenu
     * @var array
     */
    private $sub_items = [];

    private $menu_items = [];

    public function __construct($menu_items)
    {
        if(empty($menu_items)){
            return null;
        }

        $this->menu_items = $menu_items;

        $tree = [];

        // prepare tree
        foreach ($this->menu_items as $key => $item)
        {
            $tree[$item->ID] = $item;
            $tree[$item->ID]->children = [];
        }

        // make tree
        $this->menu_items = $this->createTree($tree);

        $this->makeTopMenu();

        $this->makeSubMenu();
    }

    public function getTop()
    {
        return $this->top;
    }

    public function getSubMenu()
    {
        return $this->sub_menu;
    }

    public function addTopHtml($text)
    {
        $this->top .= $text;
    }

    public function addSubMenuHtml($text)
    {
        $this->sub_menu .= $text;
    }


    private function createTree(&$elements, $parent_id = 0)
    {
        $tree = array();

        foreach ($elements as $element){
            if ($element->menu_item_parent == $parent_id)
            {
                $children = $this->createTree($elements, $element->ID);
                if($children){
                    $element->children = $children;
                }
                $tree[$element->ID] = $element;
                unset($elements[$element->ID]);
            }
        }

        return $tree;
    }

    private function makeTopMenu()
    {
        $this->addTopHtml('<section id="top-panel" class="bg-dark-blue d-flex flex-column justify-content-center border-bottom-green-1">');
            $this->addTopHtml('<div class="container">');
                $this->addTopHtml('<div class="row align-items-center">');

                    // The logo of page
                    $this->addTopHtml('<div class="col-sm-2 col-4">');
                    $this->addTopHtml(jolt_v5_get_custom_logo());
                    $this->addTopHtml('</div>');



                    $this->addTopHtml('<div class="col-sm-10 col-8 p-0">');
                        $this->addTopHtml('<div class="d-flex flex-column align-items-end font-300">');
                            $this->addTopHtml('<ul class="navbar-nav flex-row nav-item_border-right justify-content-end customer-menu" id="customer-panel">');

//                            $this->addTopHtml('<li class="nav-item"><a class="nav-link color-white nav-link_sm" href="javascript:void(0);" onclick="olark(\'api.box.expand\')">Live Chat</a></li>');
                            $this->addTopHtml('<li class="nav-item"><a class="nav-link color-white nav-link_sm" href="/support">Contact Us</a></li>');
                            $this->addTopHtml('<li class="nav-item-separate"></li>');
                            $this->addTopHtml('<li class="nav-item"><a class="nav-link color-white nav-link_sm" href="https://my.jolt.co.uk/register.php">Sign Up</a></li>');
                            $this->addTopHtml('<li class="nav-item-separate"></li>');
                            $this->addTopHtml('<li class="nav-item"><a class="nav-link color-white nav-link_sm" href="https://my.jolt.co.uk/clientarea.php">Log In</a></li>');

                            $this->addTopHtml('</ul>');

                            $this->addTopHtml('<ul class="navbar-nav flex-row justify-content-end font-500">');

                            foreach ($this->menu_items as $item){

                                $li_class = "nav-item " .implode(" ", $item->classes);
                                $a_class = 'nav-link color-white nav-link_sm dropdown-btn ';
                                $aria_container = 'aria-container="0"';
                                $icon = '';

                                if (!empty($item->children)){
                                    $aria_container = 'aria-container="#sub-menu-'. $item->ID .'"';

                                    $this->sub_items[$item->ID] = $item->children;

                                    $icon = '<span class="chevron-top-menu">
                                                                        <svg class="bi bi-chevron-down chevron-green"
                                                                             width="1em"
                                                                             height="1em" viewBox="0 0 16 16"
                                                                             fill="currentColor"
                                                                             xmlns="http://www.w3.org/2000/svg">
                                                                              <path fill-rule="evenodd"
                                                                                    d="M1.646 4.646a.5.5 0 01.708 0L8 10.293l5.646-5.647a.5.5 0 01.708.708l-6 6a.5.5 0 01-.708 0l-6-6a.5.5 0 010-.708z"
                                                                                    clip-rule="evenodd"/>
                                                                        </svg>
                                                                    </span>';
                                }



                                $this->addTopHtml('<li class="'. $li_class .'">
                                                            <a class="'. $a_class .'" href="'. $item->url .'" '. $aria_container .'>
                                                                '. $item->title .'
                                                                    '. $icon .'
                                                            </a>
                                                        </li>');

                                                    }

                            $this->addTopHtml('</ul>');


                         $this->addTopHtml('</div>');
                    $this->addTopHtml('</div>');

                $this->addTopHtml('</div>');
            $this->addTopHtml('</div>');
        $this->addTopHtml('</section>');
    }

    private function makeSubMenu()
    {

        $this->addSubMenuHtml('<section id="sub-menu" class="sub-nav border-top-green-1 fadeInDown" aria-owns="0">');
            $this->addSubMenuHtml('<div class="container">');

            foreach ($this->sub_items as $parent_id => $items)
            {
                $this->addSubMenuHtml('<div id="sub-menu-'. $parent_id .'" class="dropdown-sub-menu" __permission="'. $parent_id .'">');
                    $this->addSubMenuHtml('<div class="container">');
                        $this->addSubMenuHtml('<nav class="navbar">');

                                $this->addSubMenuHtml('<div class="collapse navbar-collapse d-flex flex-column align-items-start"">');

                                    foreach ($items as $key => $item)
                                    {


                                        $font = ' font-300';

                                        if($key == array_key_first($items)){
                                            $font = ' font-500';
                                        }

                                        $this->addSubMenuHtml('<div id="category-'. $item->ID .'">');
                                            $this->addSubMenuHtml('<div>');

                                            $this->addSubMenuHtml('<header class="category-name nav-link dropdown-btn font-400 color-green text-uppercase">'. $item->title .'</header>');

                                            $this->addSubMenuHtml('<ul class="navbar-nav flex-sm-row flex-wrap '. $font .' ">');

                                                foreach ($item->children as $child)
                                                    {
                                                        $this->addSubMenuHtml('<li class="nav-item">');

                                                            $flag_children = !empty($child->children);
                                                            $class = '';
                                                            if($flag_children){
                                                                $class = 'dropdown-btn';
                                                            }

                                                            $this->addSubMenuHtml('<a class="nav-link color-white '.$class.'" href="'. $child->url .'" __allow="'. $child->ID .'" aria-container=".sub-item-menu-'. $child->ID .'" >');
                                                            $this->addSubMenuHtml( $child->title);
                                                            if($flag_children){
                                                                $this->addSubMenuHtml('<span class="chevron-top-menu">
                                                                                                <svg    
                                                                                                        class="bi bi-chevron-right chevron-green"
                                                                                                        width="1em"
                                                                                                        height="1em" viewBox="0 0 16 16"
                                                                                                        fill="currentColor"
                                                                                                        xmlns="http://www.w3.org/2000/svg">
                                                                                                  <path fill-rule="evenodd"
                                                                                                        d="M4.646 1.646a.5.5 0 01.708 0l6 6a.5.5 0 010 .708l-6 6a.5.5 0 01-.708-.708L10.293 8 4.646 2.354a.5.5 0 010-.708z"
                                                                                                        clip-rule="evenodd"/>
                                                                                                </svg>
                                                                                            </span>');
                                                            }
                                                            $this->addSubMenuHtml('</a>');

                                                            if(!empty($child->children)){
                                                                $this->addSubMenuHtml('<div class="item-sub-menu sub-item-menu-'. $child->ID .'" __permission="'. $child->ID .'">');
                                                                    $this->addSubMenuHtml('<ul class="navbar-nav flex-sm-row flex-wrap ml-2 mt-2">');

                                                                        foreach ($child->children as $sub_child)
                                                                        {
                                                                            $this->addSubMenuHtml('<li class="nav-item">');
                                                                                $this->addSubMenuHtml('<a class="nav-link color-white" href="'. $sub_child->url .'">');
                                                                                    $this->addSubMenuHtml($sub_child->title);
                                                                                $this->addSubMenuHtml('</a>');
                                                                            $this->addSubMenuHtml('</li>');
                                                                        }

                                                                    $this->addSubMenuHtml('</ul>');
                                                                $this->addSubMenuHtml('</div>');
                                                            }



                                                        $this->addSubMenuHtml('</li>');
                                                    }

                                            $this->addSubMenuHtml('</ul>');

                                            $this->addSubMenuHtml('</div>');

                                            $this->addSubMenuHtml('<div id="sub-item-menu" aria-owns="0">');
                                                foreach ($item->children as $child){

                                                    if(!empty($child->children)){
                                                        $this->addSubMenuHtml('<div class="item-sub-menu sub-item-menu-'. $child->ID .'"  __permission="'. $child->ID .'">');

                                                            $this->addSubMenuHtml('<ul class="navbar-nav flex-sm-row flex-wrap ml-2 mt-2">');

                                                            foreach ($child->children as $sub_child)
                                                            {
                                                                $this->addSubMenuHtml('<li class="nav-item">');
                                                                $this->addSubMenuHtml('<a class="nav-link color-white" href="'. $sub_child->url .'"  __permission="'. $child->ID .'">');
                                                                $this->addSubMenuHtml($sub_child->title);
                                                                $this->addSubMenuHtml('</a>');
                                                                $this->addSubMenuHtml('</li>');
                                                            }

                                                            $this->addSubMenuHtml('</ul>');
                                                        $this->addSubMenuHtml('</div>');
                                                    }

                                                }
                                            $this->addSubMenuHtml('</div>');


                                        $this->addSubMenuHtml('</div>');
                                    }

                                $this->addSubMenuHtml('</div>');

                        $this->addSubMenuHtml('</nav>');
                    $this->addSubMenuHtml('</div>');
                $this->addSubMenuHtml('</div>');
            }

            $this->addSubMenuHtml('</div>');
        $this->addSubMenuHtml('</section>');

        $this->addSubMenuHtml('<section id="signal-close">');
        $this->addSubMenuHtml('</section>');


    }



}
?>