<?php

namespace inc\builder\column;

use inc\builder\Composite;

class Column extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
    }

    public function render(): string
    {
        $output = parent::render();

        return "<div id='{$this->getId()}' class='{$this->getClass()}' {$this->getAdditional()}>$output</div>";
    }

}