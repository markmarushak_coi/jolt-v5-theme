<?php

namespace inc\builder\faqs;

use inc\builder\Composite;

class Item extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('faq__item');
    }

    public function render(): string
    {
        $output = parent::render();

        return "<div class='{$this->getClass()}'>$output</div>";
    }

}