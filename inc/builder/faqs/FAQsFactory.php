<?php

namespace inc\builder\faqs;

use inc\builder\AbstractFactory;
use inc\builder\column\Column;
use inc\builder\Composite;
use inc\builder\row\Row;

class FAQsFactory extends AbstractFactory
{

    private $question = null;

    private $answer = null;

    public function __construct(array $data, Composite $container)
    {
        parent::__construct($data, $container);

        if (!empty($data)) {
            $this->build($data);
        }
    }

    public function reset()
    {
        $this->question = null;
        $this->answer = null;
    }

    public function fill($faq)
    {
        $this->question = $faq['question'];
        $this->answer = $faq['answer'];
    }

    protected function build($faqs)
    {

        $row = new Row(generateRandomString());
        $col = new Column(generateRandomString());
        $col->addClass('col-12');
        $row->add($col);

        $faq_block = new FAQ('faq');
        $faq_list = new Accordion('accordion');

        $faq_block->add($faq_list);
        $col->add($faq_block);


        foreach ($faqs as $faq) {
            $this->reset();
            $this->fill($faq);
            $faq_list->add($this->collect());
        }

        $this->container->add($row);

    }

    protected function collect()
    {
        $item = new Item(generateRandomString());
        $key = generateRandomString(6);

        $question = new Question(generateRandomString(), $key, $this->question);
        $answer = new Answer(generateRandomString(), $key, $this->answer);

        $question->add($answer);
        $item->add($question);

        return $item;
    }
}