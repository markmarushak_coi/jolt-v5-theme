<?php

namespace inc\builder\faqs;

use inc\builder\Composite;

class Accordion extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('faq__list');
        $this->addClass('accordion');
    }

    public function render(): string
    {
        $output = parent::render();

        return "<div class='{$this->getClass()}'>$output</div>";
    }

}