<?php

namespace inc\builder\faqs;

use inc\builder\Composite;
use inc\builder\Element;

class Question extends Composite
{

    private $question;

    private $key;

    public function __construct(string $name, $key, string $question)
    {
        parent::__construct($name);
        $this->question = $question;
        $this->key = $key;
        $this->addClass('banner__message order-2');
    }

    public function render(): string
    {
        $output = parent::render();
        return "<div id='asked-{$this->key}'
                    class='faq__item__header'
                    itemscope
                    itemprop='mainEntity'
                    itemtype='https://schema.org/Question'
                    data-target='#answer-{$this->key}'
                    aria-controls='answer-{$this->key}'>
                    <span class='item__header__name'>
                        <h3 class='faq__question' itemprop='name'>
                           {$this->question}
                        </h3>
                        <span class='faq__chevron'></span>
                    </span>
                    $output
                </div>";
    }
}
