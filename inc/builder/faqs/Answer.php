<?php

namespace inc\builder\faqs;

use inc\builder\Element;

class Answer extends Element
{

    private $answer;

    private $key;

    public function __construct(string $name, $key, string $answer)
    {
        parent::__construct($name);
        $this->answer = $answer;
        $this->key = $key;
        $this->addClass('banner__message order-2');
    }

    public function render(): string
    {
        return "<div id='answer-{$this->key}'
                        class='faq__item__body collapse'
                        aria-labelledby='asked-{$this->key}'
                        itemscope
                        itemprop='acceptedAnswer'
                        itemtype='https://schema.org/Answer'
                        data-parent='#faq'>
                    <div class='item__body__text' itemprop='text'>{$this->answer}</div>
                </div>";
    }
}