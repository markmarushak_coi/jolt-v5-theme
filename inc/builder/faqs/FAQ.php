<?php

namespace inc\builder\faqs;

use inc\builder\Composite;

class FAQ extends Composite
{
    private $meta_description = "";

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('faq__block');
    }

    public function render(): string
    {
        $output = parent::render();
        return "<div class='{$this->getClass()}' id='faq'>$output</div>";
    }

    public function addMetaDescription(array $metaDescription)
    {
        $this->meta_description =  "<!-- wp:yoast/faq-block " . json_encode($metaDescription, JSON_UNESCAPED_UNICODE) . " -->";
    }

}