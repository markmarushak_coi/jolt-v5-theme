<?php

namespace inc\builder\container;

use inc\builder\Composite;

class Container extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('container');
    }

    public function render(): string
    {
        $output = parent::render();

        return "<div class='{$this->getClass()}'>$output</div>";
    }

    public function swap(Composite $class)
    {
        return $class->add($this);
    }

}