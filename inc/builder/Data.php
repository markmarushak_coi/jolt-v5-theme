<?php

namespace inc\builder;


class Data
{
    protected $data;

    public function __construct(array $data)
    {
        $this->setData($data);
    }

    /**
     * @return array
     */
    public function getData($key = null)
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        if(empty($data)){
            return null;
        }

        $this->data = $data;
    }

    public function hasData()
    {
        return !empty($this->getData());
    }

    public function removeKey($key)
    {
        $this->data = array_filter($this->data, function ($_key) use ($key) {
            return $key != $_key;
        }, ARRAY_FILTER_USE_KEY );
    }

    public function getDataByKey($key)
    {
        $result = null;

        array_walk_recursive($this->getData(), function ($item, $_key, $result) use ($key){
            if($key == $_key){
                $result = $item;
            }
        }, $result);

        if(empty($result)){
            return null;
        }

        return $result;
    }
}