<?php

namespace inc\builder\banner;


use inc\builder\Composite;

class BannerLeft extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('banner__left');
        $this->addClass('d-lg-flex');
        $this->addClass('d-none');
    }

    public function render(): string
    {
        $output = parent::render();

        return "<div class='{$this->getClass()}'>$output</div>";
    }

}