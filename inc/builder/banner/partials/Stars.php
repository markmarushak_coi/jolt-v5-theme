<?php

namespace inc\builder\banner\partials;


use inc\builder\Element;

class Stars extends Element
{

    private $stars = 0;

    public function __construct(string $name, $stars = 5)
    {
        parent::__construct($name);
        $this->addClass('banner__stars');
        $this->stars = $stars;
    }

    public function render(): string
    {
        $output = '';

        for ($i = 0; $i < $this->stars; $i++){
            $output .= '<span class="star star-xl"></span>';
        }

        return "<div class='{$this->getClass()}'>$output</div>";
    }

}