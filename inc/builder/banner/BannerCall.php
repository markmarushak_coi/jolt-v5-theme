<?php

namespace inc\builder\banner;

use inc\builder\Composite;

class BannerCall extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('banner__call');

        return $this;
    }

    public function render(): string
    {
        $output = parent::render();

        return "<div class='{$this->getClass()}'>$output</div>";
    }

}