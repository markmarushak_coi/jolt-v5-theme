<?php

namespace inc\builder\banner;


use inc\builder\Composite;

class Banner extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('banner__block');
    }

    public function render(): string
    {
        $output = parent::render();

        return "<div class='{$this->getClass()}'>$output</div>";
    }

}