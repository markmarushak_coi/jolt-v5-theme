<?php

namespace inc\builder\banner;

use inc\builder\banner\partials\Stars;
use inc\builder\Composite;
use inc\builder\Element;

class BannerHeader extends Composite
{

    public function __construct(string $name, bool $stars)
    {
        parent::__construct($name);
        $this->addClass('banner__header order-1');

        if($stars){
            $this->add(new Stars('stars'));
        }
    }

    public function render(): string
    {
        $output = parent::render();

        return "<div class='{$this->getClass()}'>$output</div>";
    }
}