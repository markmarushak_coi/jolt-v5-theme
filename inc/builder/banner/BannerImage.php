<?php

namespace inc\builder\banner;


use inc\builder\Element;

class BannerImage extends Element
{

    private $link;

    private $alt;

    public function __construct(string $name, string $link, string $alt = '')
    {
        parent::__construct($name);
        $this->link = $link;
        $this->alt = $alt;
        $this->addClass('banner__img');
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'><img src='{$this->link}' alt='{$this->alt}' loading='lazy'></div>";
    }
}