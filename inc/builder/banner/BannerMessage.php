<?php

namespace inc\builder\banner;

use inc\builder\Element;

class BannerMessage extends Element
{

    private $message;

    public function __construct(string $name, string $message)
    {
        parent::__construct($name);
        $this->message = $message;
        $this->addClass('banner__message order-2');
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'>{$this->message}</div>";
    }
}