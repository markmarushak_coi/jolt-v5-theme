<?php

namespace inc\builder\banner;

use inc\builder\Element;

class BannerButton extends Element
{

    private $text;

    private $link;

    public function __construct(string $name, string $text, string $link)
    {
        parent::__construct($name);
        $this->text = $text;
        $this->link = $link;
        $this->addClass('banner__button order-3');
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'><a href='{$this->link}' class='btn btn-custom btn-green font-600'>{$this->text}</a></div>";
    }
}