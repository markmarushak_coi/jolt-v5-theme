<?php

namespace inc\builder\banner;

use inc\builder\background\BackgroundContainer;
use inc\builder\banner\partials\Stars;
use inc\builder\Composite;
use inc\builder\container\Container;
use inc\builder\row\Row;

class BannerFactory
{

    private $button = null;

    private $message = null;

    private $image = null;

    private $star = null;

    private $background = null;
    private $background_type = null;

    private $header = [];

    public $html = '';

    private $section;

    private $container;


    public function __construct($data, Composite $container)
    {
        if(empty($data)){
            return [];
        }

        $this->container = $container;

        $this->render($data);

    }

    public function reset()
    {
        $this->image = null;
        $this->button = null;
        $this->message = null;
        $this->star = null;
        $this->header = [];
    }

    public function make($banner)
    {
        $this->image = $banner['banner_image'];
        $this->button = $banner['button'];
        $this->message = $banner['messages'];


        $background = $banner['background'];

        $this->background_type = $background['type'];
        $this->background = $background[$this->background_type];

        $this->header = $banner['header'];

        if(isset($banner['header']['stars'])){
            $this->star = $banner['header']['stars'];
        }
    }

    public function addHtml($text)
    {
        $this->html .= $text;
    }

    private function render($data)
    {

        $this->reset();
        $this->make($data);

        $banner = $this->show();

        $row = new Row('row');

        $row->add($banner);

        if(!empty($this->background)){

            $background = new BackgroundContainer(generateRandomString(), $this->background, $this->background_type);

            $background->addClass('d-flex flex-column justify-content-center align-items-center');

            // swap class container and backgroundContainer
            $background->add($this->container);

            $this->container::create('container', $background);


        }

        $this->container->add($row);

    }

    private function show()
    {
        $banner = new Banner('banner');

        // Banner Left
        if(!empty($this->image)){
            $banner_left = new BannerLeft('left');

            $banner_left->add(new BannerImage('image', $this->image, $this->message));

            $banner->add($banner_left);
        }


        // Banner Right
        $banner_right = new BannerRight('right');

        $call = new BannerCall('call');

        $banner_right->add($call);

        if(!empty($this->message)){
            $call->add(new BannerMessage('message', $this->message));
        }

        if(!empty($this->star)){
            $stars = new Stars('stars');

            $header = new BannerHeader('header', 1);

            $header->add($stars);

            $call->add($header);
        }

        if(!empty($this->button)){
            $call->add(new BannerButton('button', $this->button['text'], $this->button['link']));
        }

        $banner->add($banner_right);

        return $banner;
    }
}