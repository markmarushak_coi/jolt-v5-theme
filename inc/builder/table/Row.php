<?php

namespace inc\builder\table;

use inc\builder\Composite;

class Row extends Composite
{
    public function __construct(string $name)
    {
        parent::__construct($name);
    }

    public function render(): string
    {
        $output = parent::render();
        return "<tr class='{$this->getClass()}' id='{$this->getId()}'>$output</tr>";
    }
}