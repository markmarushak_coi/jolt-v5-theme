<?php

namespace inc\builder\table;

use inc\builder\Element;

class Td extends Element
{
    private $html;

    public function __construct(string $name, $html)
    {
        parent::__construct($name);
        $this->html = $html;
    }

    public function render(): string
    {
        return "<td class='{$this->getClass()}' id='{$this->getId()}'>{$this->html}</td>";
    }
}