<?php

namespace inc\builder\table;

use inc\builder\Element;

class Th extends Element
{
    private $html;

    public function __construct(string $name, $html)
    {
        parent::__construct($name);
        $this->html = $html;
    }

    public function render(): string
    {
        return "<th class='{$this->getClass()}' id='{$this->getId()}'>{$this->html}</th>";
    }
}