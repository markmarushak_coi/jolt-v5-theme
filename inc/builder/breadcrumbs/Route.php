<?php


namespace inc\builder\breadcrumbs;

use inc\builder\Element;

class Route extends Element
{
    protected $link;

    public function __construct(string $name, string $link, bool $current)
    {
        parent::__construct($name);

        $this->link = $link;

        $this->addClass('breadcrumbs__route');

        if($current){
            $this->addClass('breadcrumbs__route-current');
        }
    }

    public function render(): string
    {
        return "<a href='{$this->link}' class='{$this->getClass()}'>{$this->name}</a>";
    }

}