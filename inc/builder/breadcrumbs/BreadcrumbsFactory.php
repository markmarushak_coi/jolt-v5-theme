<?php

namespace inc\builder\breadcrumbs;

use inc\builder\AbstractFactory;
use inc\builder\Composite;
use inc\builder\Element;

class BreadcrumbsFactory extends AbstractFactory
{
    private $routes = [];

    private $current_post = null;

    private $is_current = false;

    private $route_name;

    private $route_link = "";

    private $parent_link = '';

    public function __construct(array $data, Composite $container)
    {
        parent::__construct($data, $container);

        $this->current_post = get_post();

        if (empty($this->current_post)) {
            return null;
        }

        $this->route_link = $this->getList($this->current_post);

        if (count($this->routes) > 1) {
            $this->build(array_reverse($this->routes));
        }
    }

    protected function fill($route)
    {
        $this->route_name = $route->post_title;

        $this->route_link = $this->getUrl($route->post_name);

        $this->is_current = $route->ID === $this->current_post->ID ?? false;

        $this->parent_link = $this->route_link;

    }

    protected function build(array $routes)
    {
        $breadcrumbs = new Breadcrumbs();

        foreach ($routes as $key => $route) {
            $this->fill($route);
            $route = $this->collect();
            $breadcrumbs->add($route);

            if ($key != array_key_last($routes)){
                $breadcrumbs->add(new Separate());
            }
        }

        $row = $this->createRow();
        $row->removeClass('justify-content-center');
        $row->addClass('align-items-center');

        $row->add($breadcrumbs);

        $this->container->add($row);
    }

    protected function collect(): Element
    {
        return new Route($this->route_name, $this->route_link, $this->is_current);
    }


    private function getList(\WP_Post $post)
    {
        array_push($this->routes, $post);

        if ($post->post_parent > 0) {
            $this->getList(get_post($post->post_parent));
        }

        return 0;
    }

    private function getUrl($slug)
    {
        $url = [
            $this->parent_link,
            $slug,
        ];
        return implode("/", $url);
    }
}