<?php


namespace inc\builder\breadcrumbs;


use inc\builder\Element;

class Separate extends Element
{
    public function __construct()
    {
        parent::__construct(generateRandomString());

        $this->addClass('breadcrumbs__separate');
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'></div>";
    }
}