<?php

namespace inc\builder\breadcrumbs;

use inc\builder\Composite;

class Breadcrumbs extends Composite
{

    public function __construct()
    {
        parent::__construct(generateRandomString());
    }

    public function render(): string
    {
        $output = parent::render();

        return "<div class='breadcrumbs'>$output</div>";
    }

}