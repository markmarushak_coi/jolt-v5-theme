<?php

namespace inc\builder\background;

use inc\builder\Composite;

class BackgroundContainer extends Composite
{
    const UPLOAD_IMG = 'upload_image';
    const CUSTOM_IMG = 'custom_class';

    private $type;

    private $bg;

    public function __construct(string $name, $background, $type)
    {
        parent::__construct($name);
        $this->type = $type;
        $this->bg = $background;
    }

    public function render(): string
    {
        $output = parent::render();

        switch ($this->type) {
            case self::UPLOAD_IMG:
                return "<div class='{$this->getClass()} background_banner' style='background-image: url({$this->bg})'>$output</div>";
            case self::CUSTOM_IMG:
                $this->addClass($this->bg);
                return "<div class='{$this->getClass()}'>$output</div>";
        }

    }

    public function setImage()
    {
        
    }

}