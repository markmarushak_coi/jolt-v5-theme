<?php

namespace inc\builder\background;

use inc\builder\Element;

class Background extends Element
{

    private $background_name;

    private $output = '';

    private $image = false;

    public function __construct(string $name, $background_name, $image = false)
    {
        parent::__construct($name);
        $this->background_name = $background_name;

        if($image){
            $this->output .= "<img src='{$this->background_name}' alt='jolt background web hosting' loading='lazy'>";
        }else{
            $this->addClass($this->background_name);
        }

        $this->addClass('background');
    }

    public function render(): string
    {
        if($this->image){
            return "<div class='{$this->getClass()}'><img class='img' src='{$this->background_name}' alt='jolt background web hosting' loading='lazy'></div>";
        }

        return "<div class='{$this->getClass()}'></div>";
    }

}