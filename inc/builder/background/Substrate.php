<?php

namespace inc\builder\background;

use inc\builder\Element;

class Substrate extends Element
{

    public function __construct(string $name, $class = [])
    {
        parent::__construct($name);
        $this->addClass('substrate');
        $this->addClass('d-sm-block d-none');

        if(!empty($class)){
            $this->addClass($class);
        }
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'></div>";
    }

}