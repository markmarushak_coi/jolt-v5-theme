<?php

namespace inc\builder\header;

use inc\builder\Element;

class Image extends Element
{

    private $url;

    private $alt;

    public function __construct(string $name, string $url, string $alt = '')
    {
        parent::__construct($name);
        $this->url = $url;
        $this->alt = $alt;
        $this->addClass('header__picture');
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'>
                    <img class='img-100x100' src='{$this->url}' alt='{$this->alt}' loading='lazy'>
                </div>";
    }

}