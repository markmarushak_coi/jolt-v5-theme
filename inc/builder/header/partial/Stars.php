<?php

namespace inc\builder\header\partial;

use inc\builder\Element;

class Stars extends Element
{

    public function __construct(string $name)
    {
        parent::__construct($name);
    }

    public function render(): string
    {
        return "<span class='d-flex'>
                    <span class='star'></span>
                    <span class='star'></span>
                    <span class='star'></span>
                    <span class='star'></span>
                    <span class='star'></span>
                </span>

                <span class='color-white'>Rated 5* by our customers for over ". COMPANY_YEARS ." years</span>";
    }

}