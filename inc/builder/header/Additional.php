<?php

namespace inc\builder\header;

use inc\builder\Composite;
use inc\builder\header\partial\Stars;

class Additional extends Composite
{

    private $partial = [
      'stars' => Stars::class
    ];

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass(['header__additional', 'col-12']);
    }

    public function render(): string
    {
        $output = parent::render();
        return "<div class='{$this->getClass()}'>$output</div>";
    }

    public function createPartial($name)
    {
        if(isset($this->partial[$name])){
            $class = $this->partial[$name];
            return new $class(generateRandomString());
        }

        return null;
    }


}