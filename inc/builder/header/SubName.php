<?php

namespace inc\builder\header;

use inc\builder\Element;

class SubName extends Element
{
    private $text;

    public function __construct(string $name, string $text)
    {
        parent::__construct($name);
        $this->addClass('header__title__sub');
        $this->text = $text;
    }

    public function render(): string
    {
        return "<span class='{$this->getClass()}'>{$this->text}</span>";
    }
}