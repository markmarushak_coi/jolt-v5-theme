<?php

namespace inc\builder\header;

use inc\builder\Composite;

class Header extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->setId('header');
    }

    public function render(): string
    {
        $output = parent::render();
        return "<section id='{$this->getId()}' class='{$this->getClass()}'>$output</section>";
    }

}