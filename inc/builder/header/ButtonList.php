<?php

namespace inc\builder\header;

use inc\builder\Composite;

class ButtonList extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('buttons__list');
    }

    public function render(): string
    {
        $output = parent::render();
        return "<div class='{$this->getClass()}'>$output</div>";
    }

}