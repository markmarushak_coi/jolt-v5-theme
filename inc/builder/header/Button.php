<?php

namespace inc\builder\header;

use inc\builder\Element;

class Button extends Element
{

    private $url;

    private $text;

    public function __construct(string $name, string $url, string $text = '')
    {
        parent::__construct($name);
        $this->url = $url;
        $this->text = $text;
        $this->addClass('buttons__item');
    }

    public function render(): string
    {
        return "<a href='{$this->url}' class='{$this->getClass()}'>{$this->text}</a>";
    }

}