<?php

namespace inc\builder\header;

use inc\builder\breadcrumbs\BreadcrumbsFactory;
use inc\builder\AbstractFactory;
use inc\builder\background\Background;
use inc\builder\Composite;
use inc\builder\container\Container;
use inc\builder\Data;

class HeaderFactory extends AbstractFactory
{

    private $name;
    private $sub_name;
    private $description;
    private $additional = [];
    private $buttons = [];
    private $image;

    public function __construct(array $data, Composite $container)
    {
        parent::__construct($data, $container);

        if(!empty($data)){
            foreach ($data as $item){
                $data = $item[$item['acf_fc_layout']];
            }

            if(isset($this->modifications['header'])){
                $this->container->addClass($this->modifications['header']);
            }

            $this->background($data);

            $this->build($data);
        }
    }

    protected function fill($data)
    {
        $this->name = $data['title'];
        $this->sub_name = $data['sub_title'];
        $this->image = $data['image'];
        $this->description = $data['description']['text'];
        if(is_array($data['buttons'])){
            array_filter($data['buttons'], function ($item){
                if(!empty($item['text']) && !empty($item['link'])){
                    array_push($this->buttons, $item);
                }
            });
        }

        $this->additional = $data['additional'];
    }

    protected function build($data)
    {

        $container = new Container(generateRandomString());

        // add to container are breadcrumbs
        new BreadcrumbsFactory([], $container);

        $row = $this->createRow();
        $row->removeClass('justify-content-center');
        $container->add($row);
        $col = $this->createColumn();
        $col->addClass(['col-md-7', 'col']);
        $row->add($col);

        $this->reset();
        $this->fill($data);
        $header = $this->collect();
        $col->add($header);

        $row->add(new Image(generateRandomString(), $this->image, $this->name));

        $this->container->add($container);
    }

    protected function collect()
    {



        $header = $this->createRow();
        $header->removeClass('justify-content-center');

        if(!empty($this->name)){
            $title = new Title(generateRandomString());
            $title->addClass([$this->getClass('title'), 'col-12']);

            $name = new Name(generateRandomString(), $this->name);
            $name->addClass($this->getClass('name'));
            $title->add($name);

            $sub_name = new SubName(generateRandomString(), $this->sub_name);
            $sub_name->addClass($this->getClass('sub_name'));
            $title->add($sub_name);

            $header->add($title);
        }

        $col = $this->createColumn();
        $col->addClass(['offset-1', 'col']);
        $header->add($col);


        $row = $this->createRow(); // new row
        $row->removeClass('justify-content-center');
        $col->add($row);

        if(!empty($this->description)){

            $description = new Description(generateRandomString(), $this->description);
            $description->addClass($this->getClass('description'));

            $row->add($description);
        }

        if(!empty($this->additional)){

            $additional = new Additional(generateRandomString());
            $additional->addClass($this->getClass('additional'));

            foreach ($this->additional as $partial){
                $partial = $additional->createPartial(array_pop($partial));

                if(!empty($partial)){
                    $additional->add($partial);
                }
            }

            $row->add($additional);
        }

        if(!empty($this->buttons)){
            $button_group = new ButtonGroup(generateRandomString());
            $button_list = new ButtonList(generateRandomString());
            $button_group->add($button_list);

            foreach ($this->buttons as $button){
                $btn_class = $button['modifications'];
                $button = new Button(generateRandomString(), $button['link'], $button['text']);
                $button->addClass($btn_class);
                $button_list->add($button);
            }

            $row->add($button_group);
        }


        return $header;
    }

    public function render()
    {
        echo $this->container->render();
    }

    private function background($data)
    {
        $flag = $data['background'];

        if(!empty($flag) && isset($data[$flag])){
            $background = new Background('background', $data[$flag], strpos($data[$flag], '://'));
            $background->addClass([
                'background__header',
                'header__background'
            ]);

            $this->container->add($background);
        }

        unset($data['background'], $data['background_image'], $data['background_class']);
    }

}