<?php

namespace inc\builder\header;

use inc\builder\Element;

class Description extends Element
{
    private $text;

    public function __construct(string $name, string $text)
    {
        parent::__construct($name);
        $this->addClass(['header__description', 'col-12']);
        $this->text = $text;
    }

    public function render(): string
    {
        return "<h2 class='{$this->getClass()}'>{$this->text}</h2>";
    }
}