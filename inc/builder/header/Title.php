<?php

namespace inc\builder\header;

use inc\builder\Composite;

class Title extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('header__title');
    }

    public function render(): string
    {
        $output = parent::render();
        return "<h1 class='{$this->getClass()}'>$output</h1>";
    }

}