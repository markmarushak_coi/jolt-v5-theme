<?php

namespace inc\builder\header;

use inc\builder\Composite;

class ButtonGroup extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('header__buttons col-12');
    }

    public function render(): string
    {
        $output = parent::render();
        return "<div class='{$this->getClass()}'>$output</div>";
    }

}