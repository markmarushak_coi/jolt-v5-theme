<?php

namespace inc\builder\product;

use inc\builder\Composite;

class Product extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('product__card');
    }

    public function render(): string
    {
        $output = parent::render();

        return "<div class='{$this->getClass()}'>$output</div>";
    }

}