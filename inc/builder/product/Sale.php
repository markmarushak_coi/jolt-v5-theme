<?php

namespace inc\builder\product;

use inc\builder\Element;

class Sale extends Element
{

    private $sale;

    public function __construct(string $name, \WP_Term $sale, $order = false)
    {
        parent::__construct($name);
        $this->sale = $sale;
        $this->addClass('product__sale');

        if($order !== false) {
            $this->addClass('order-'.$order);
        }else{
            $this->addClass('order-0');
        }
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'>{$this->sale->name}</div>";
    }
}