<?php

namespace inc\builder\product;

use inc\builder\Composite;
use inc\builder\Element;

class AdditionalOpen extends Element
{

    public function __construct(string $name, $order = false)
    {
        parent::__construct($name);
        $this->addClass('additional__open');

        if($order !== false) {
            $this->addClass('order-'.$order);
        }else{
            $this->addClass('order-0');
        }
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'>
                    <span class='open'>See features <i class='chevron-right-green'></i></span>
                    <span class='back'>Back product <i class='chevron-left-green'></i></span>
                </div>";
    }
}