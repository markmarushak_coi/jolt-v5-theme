<?php

namespace inc\builder\product;

use inc\builder\Element;

class Name extends Element
{

    private $product_name;

    public function __construct(string $name, string $product_name)
    {
        parent::__construct($name);
        $this->product_name = $product_name;
        $this->addClass('product__header__name order-2');
    }

    public function render(): string
    {
        return "<h2 class='{$this->getClass()}'>{$this->product_name}</h2>";
    }
}