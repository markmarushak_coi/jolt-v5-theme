<?php

namespace inc\builder\product;

use inc\builder\Composite;
use inc\builder\Element;

class AdditionalBody extends Element
{

    private $html;

    public function __construct(string $name, $order = false)
    {
        parent::__construct($name);
        $this->addClass('additional__body text-center');

//        $this->html = $html;

        if($order !== false) {
            $this->addClass('order-'.$order);
        }else{
            $this->addClass('order-0');
        }
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'>{$this->getData()}</div>";
    }
}