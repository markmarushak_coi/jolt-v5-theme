<?php

namespace inc\builder\product;

use inc\builder\Element;

class BodyDescription extends Element
{

    private $html;

    public function __construct(string $name, $html, $order = false)
    {
        parent::__construct($name);
        $this->html = $html;
        $this->addClass('product__description');

        if($order !== false) {
            $this->addClass('order-'.$order);
        }else{
            $this->addClass('order-0');
        }
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'>{$this->html}</div>";
    }
}