<?php

namespace inc\builder\product;

use inc\builder\Element;

class HeaderTop extends Element
{

    private $message;

    public function __construct(string $name, string $message)
    {
        parent::__construct($name);
        $this->message = $message;
        $this->addClass('product__header__top order-1');
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'>{$this->message}</div>";
    }
}