<?php

namespace inc\builder\product;

use inc\builder\AbstractFactory;
use inc\builder\Composite;
use inc\builder\row\Row;
use Spatie\SchemaOrg\Schema;


class ProductFactory extends AbstractFactory
{

    // header
    private $product_name = null;

    private $before_name = null;


    // body
    private $price = null;

    private $description = null;

    private $sale = null;

    private $button = [];

    private $additional_info = null;

    // Advanced Features
    private $advanced_feature = null;

    // special offer show first
    private $special_offer;

    private $special_offer_settings = [];

    private $product_modifications = [];

    public function __construct($data, Composite $container)
    {

        foreach ($data as $key => $item) {
            if ($item['acf_fc_layout'] == 'special_offer') {
                $this->special_offer = $item['text'];
                if (!empty($item['modifications'])) {
                    $this->special_offer_settings = $item['modifications'];
                }
                unset($data[$key]);
                break;
            }
        }

        parent::__construct($data, $container);

        if (!empty($data)) {
            $this->addSpecialOffers();
        }

        if(!empty($data)){
            $this->build($data);
        }

    }

    public function reset()
    {
        $this->product_name = null;
        $this->price = null;
        $this->description = null;
        $this->sale = null;
        $this->advanced_feature = null;
        $this->before_name = null;
        $this->additional_info = null;
        $this->button = [];
        $this->product_modifications = [];
    }

    public function fill($product)
    {

        foreach ($product as $block) {
            if (method_exists($this, $block['acf_fc_layout'])) {// to check is method
                $result = call_user_func([$this, $block['acf_fc_layout']], $block); // call method by name
            }
        }

    }

    private function header($block)
    {
        $this->product_name = $block['product_name'];
        $this->before_name = $block['head'];
    }

    private function body($block)
    {
        $this->description = $block['description'];
        $this->sale = $block['sale'];
        $this->price = $block['price'];
        $this->additional_info = $block['additional_info'];


        if (!empty($block['button']['text']) and !empty($block['button']['link'])) {
            $this->button = $block['button'];

            if (!$this->button['edit_button']) {
                unset($this->button['modifications']);
            }
        }
    }

    private function product_modifications($modifications)
    {
        foreach ($modifications['product_modifications'] as $modification) {

            $value = array_pop($modification);
            $name = array_pop($modification);

            if (!isset($this->product_modifications[$name])) {
                $this->product_modifications[$name] = [];
            }

            if (is_array($value)) {
                array_push($this->product_modifications[$name], array_pop($value));
            } else {
                array_push($this->product_modifications[$name], $value);
            }

        }
    }

    private function getProductClass($name)
    {
        if (empty($this->product_modifications[$name])) {
            return null;
        }

        return implode(' ', $this->product_modifications[$name]);
    }

    private function advanced_features($block)
    {
        $this->advanced_feature = $block['description'];
    }

    public function build($products)
    {

        $schema = \Spatie\SchemaOrg\Schema::product()
            ->name("Shared Hosting")
            ->image([
                "https://www.jolt.co.uk/wp-content/uploads/2020/08/Group-35713.png"
            ])
            ->description("Best UK Host, Rated 4.9*on Review Centre. UK Servers, Cloud Based, cPanel Shared Hosting")
            ->sku("0446310786")
            ->mpn("925872")
            ->brand("Jolt")
            ->review(
                Schema::review()
                    ->reviewRating(
                        Schema::rating()
                            ->ratingValue('4')
                            ->bestRating('5')
                    )
                    ->author(
                        Schema::person()
                            ->name('Jon Makintosh')
                    )
            )
            ->aggregateRating(
                Schema::aggregateRating()
                    ->ratingValue("4.4")
                    ->reviewCount("89")
            )
            ->offers(
                Schema::aggregateOffer()
                    ->offerCount("5")
                    ->lowPrice("14.95")
                    ->price("49.95")
                    ->highPrice("99.95")
                    ->priceCurrency("GBP")
            );
        echo $schema->toScript();

        $row = new Row('row');

        foreach ($products as $key => $product) {

            if (!isset($product['product'])) {
                continue;
            }

            // clear all property the class
            $this->reset();

            // make the class properties
            $this->fill($product['product']);

            // render
            $product = $this->collect();

            $row->add($product);

        }

        $this->container->add($row);

    }

    protected function collect()
    {
        $product = new Product(generateRandomString());

        if (!empty($this->getProductClass('product_card'))) {
            $product->addClass($this->getProductClass('product_card'));
        }

        if (!empty($this->getClass('grid'))) {
            $product->addClass($this->getClass('grid'));
        }

        $header = new Header('header');

        $product->add($header);

        if (!empty($this->before_name)) {
            $header->add(new HeaderTop('top', $this->before_name));

            if (!empty($this->getProductClass('top'))) {
                $header->addClass($this->getProductClass('top'));
            }

        }

        if (!empty($this->product_name)) {
            $header->add(new Name('name', $this->product_name));
        }

        $body = new Body('body');

        $product->add($body);

        if (!empty($this->price)) {
            $price = new BodyPrice('price', $this->price['order']);
            $body->add($price);

            $price->add(new PriceText('price_text', $this->price['text_with_price']));
            $price->add(new PriceValue('price_value', $this->price['price']));
        }

        if (!empty($this->description)) {
            $description = new BodyDescription('description', $this->description['text'], $this->description['order']);
            if (!empty($this->getClass('description'))) {
                $description->addClass($this->getClass('description'));
            }
            $body->add($description);
        }

        if (!empty($this->sale['show']) && $this->sale['show']) {
            $body->add(new Sale('sale', $this->sale['sale'], $this->sale['order']));
        }

        if (!empty($this->button)) {
            $button_group = new ButtonGroup('button_group', $this->button['order']);

            $body->add($button_group);

            $button = new Button(generateRandomString(), $this->button['link'], $this->button['text']);

            if (!empty($this->button['modifications'])) {
                foreach ($this->button['modifications'] as $class) {
                    if (isset($class[$class['name']])) {
                        $button->addClass($class[$class['name']]);
                    }
                }
            }

            $button_group->add($button);
        }

        if (!empty($this->additional_info)) {
            $body->add(new BodyMore('body_more', $this->additional_info));
        }

        if (!empty($this->advanced_feature)) {
            $advanced = new Additional('additional');

            $advanced->add(new AdditionalOpen('open'));
            $advanced->add(new AdditionalBody('body'));

            $advanced->setData([
                'body' => $this->advanced_feature
            ]);

            if (!empty($this->button)) {
                $button = new Button('button', $this->button['link'], $this->button['text']);

                $advanced->setData([
                    'body' => $button->render()
                ]);
            }

            $product->add($advanced);
        }


        return $product;
    }

    private function addSpecialOffers()
    {
        if (!empty($this->special_offer)) {
            $row = $this->createRow();

            $special_offer = new SpecialOffer(generateRandomString(), $this->special_offer);

            $row->add($special_offer);

            if (!empty($this->special_offer_settings)) {
                foreach ($this->special_offer_settings as $setting) {
                    $special_offer->addClass($setting[$setting['key']]);
                }
            }


            $this->container->add($row);
        }
    }

}