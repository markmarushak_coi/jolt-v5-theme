<?php

namespace inc\builder\product;

use inc\builder\Element;

class Button extends Element
{

    private $url;

    private $text;

    public function __construct(string $name, string $url, string $text = '')
    {
        parent::__construct($name);
        $this->url = $url;
        $this->text = $text;
        $this->addClass('btn btn-custom btn-green font-600');
    }

    public function render(): string
    {
        return "<a href='{$this->url}' class='{$this->getClass()}'>{$this->text}</a>";
    }

}