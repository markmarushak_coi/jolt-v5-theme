<?php

namespace inc\builder\product;

use inc\builder\Element;

class Message extends Element
{

    private $message;

    public function __construct(string $name, string $message)
    {
        parent::__construct($name);
        $this->message = $message;
        $this->addClass('product__card__message');
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'>
                <span class='text-uppercase'><b>Special Offer: </b>{$this->message}</span></div>";
    }
}