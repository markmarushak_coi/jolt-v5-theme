<?php

namespace inc\builder\product;

use inc\builder\Element;

class PriceText extends Element
{

    private $text;

    public function __construct(string $name, string $text)
    {
        parent::__construct($name);
        $this->text = $text;
        $this->addClass('price__text order-1');
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'>{$this->text}</div>";
    }
}