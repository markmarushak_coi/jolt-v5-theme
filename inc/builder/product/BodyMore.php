<?php

namespace inc\builder\product;

use inc\builder\Element;

class BodyMore extends Element
{

    private $html;

    public function __construct(string $name, $html)
    {
        parent::__construct($name);
        $this->html = $html;
        $this->addClass('body__more col order-9 color-dark mt-3');
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'>{$this->html}</div>";
    }
}