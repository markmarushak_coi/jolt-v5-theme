<?php

namespace inc\builder\product;

use inc\builder\Composite;

class ButtonGroup extends Composite
{

    public function __construct(string $name, $order = false)
    {
        parent::__construct($name);
        $this->addClass('product__button');

        if($order !== false) {
            $this->addClass('order-'.$order);
        }else{
            $this->addClass('order-0');
        }
    }

    public function render(): string
    {
        $output = parent::render();

        return "<div class='{$this->getClass()}'>$output</div>";
    }
}