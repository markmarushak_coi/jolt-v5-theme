<?php

namespace inc\builder\product;

use inc\builder\Element;

class PriceValue extends Element
{

    private $value;

    public function __construct(string $name, $value)
    {
        parent::__construct($name);
        $this->value = $value;
        $this->addClass('price__value order-2');
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'>{$this->value}</div>";
    }
}