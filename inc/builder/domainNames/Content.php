<?php

namespace inc\builder\domainNames;

use inc\builder\Composite;

class Content extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('tab-content');
    }

    public function render(): string
    {
        $output = parent::render();

        return "<div id='{$this->getId()}' class='{$this->getClass()}'>$output</div>";
    }

}