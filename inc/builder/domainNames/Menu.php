<?php

namespace inc\builder\domainNames;

use inc\builder\Composite;

class Menu extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('nav');
    }

    public function render(): string
    {
        $output = parent::render();
        return "<ul class='{$this->getClass()}' role='tablist'>$output</ul>";
    }

}