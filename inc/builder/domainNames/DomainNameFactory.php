<?php

namespace inc\builder\domainNames;

use inc\builder\AbstractFactory;
use inc\builder\Composite;
use inc\builder\table\Row;
use inc\builder\table\Table;
use inc\builder\table\Td;
use inc\builder\table\Th;

class DomainNameFactory extends AbstractFactory
{

    private $item_menu;


    private $menu;

    private $content;

    private $header = [];

    private $table;

    public function __construct(array $data, Composite $container)
    {
        parent::__construct($data, $container);

        if (!empty($data)){
            $this->build($data);
        }
    }

    protected function reset()
    {
        $this->item_menu = null;
        $this->table = [];
    }

    protected function fill($post, $post_id = null)
    {
        $this->item_menu = $post->post_title;

        $table_data = json_decode($post->post_content);

        $this->header = array_shift($table_data);
        $this->table = $table_data;
    }

    protected function build(array $data)
    {

        $content_id = generateRandomString(5);
        $menu_id = generateRandomString(5);

        $this->menu = new Menu(generateRandomString());
        $this->menu->setId($menu_id)->addClass(['nav-tabs']);
        $this->menu->addClass($this->getClass('menu'));


        $this->content = new Content(generateRandomString());
        $this->content->addClass('tab-content')->setId($content_id);
        $this->content->addClass($this->getClass('content'));


        $domain_name_block = $this->createColumn();
        $domain_name_block->addClass(['domain_name']);
        $domain_name_block->add($this->menu);
        $domain_name_block->add($this->content);

        $this->container->add($domain_name_block);

        foreach ($data as $key => $item){
            $this->reset();

            $post = $this->getPost($item);

            $this->fill($post, $post->ID);

            $this->collect( $post->ID, !$key? true : false );
        }


    }

    protected function collect($post_id = null, $active = false)
    {
        $item = new MenuItem(generateRandomString(), $this->item_menu, 'content-'.$post_id, $active, $active);
        $item->setId('tab-'.$post_id);
        $this->getMenu()->add($item);

        $table = new Table(generateRandomString());
        $table->addClass(['table']);

        $thead = new Row(generateRandomString());
        $table->add($thead);
        foreach ($this->header as $th){
            $thead->add(new Th(generateRandomString(), $th));
        }

        foreach ($this->table as $row) {
            $data = new Row(generateRandomString());

            foreach ($row as $order => $td){

                if($td === "0" || $td === "1"){

                    if($td === "1"){
                        $td = '<i class="fas fa-check"></i>';
                    }else{
                        $td = '<i class="fas fa-times"></i>';
                    }

                }

                $data->add(new Td(generateRandomString(), $td));
            }

            $table->add($data);
        }

        $container = $this->createColumn();
        $container->addClass(['tab-pane', 'fade', 'table-responsive-xl']);
        if($active){
            $container->addClass(['active', 'show']);
        }

        $container->setId('content-'.$post_id);
        $container->setAdditional('role="tabpanel"');
        $container->setAdditional('aria-labelledby="tab-'.$post_id.'"');
        $container->add($table);

        $this->getContent()->add($container);

    }

    private function getPost($data) : \WP_Post
    {
        return $data;
    }

    public function render()
    {
        $this->container->add($this->getMenu());
        $this->container->add($this->getContent());
    }

    /**
     * @return Composite
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * @return Composite
     */
    public function getContent()
    {
        return $this->content;
    }


}