<?php

namespace inc\builder\domainNames;

use inc\builder\Composite;

class DomainName extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('tab__block');
    }

    public function render(): string
    {
        $output = parent::render();
        return "<div class='{$this->getClass()}'>$output</div>";
    }

}