<?php

namespace inc\builder\domainNames;

use inc\builder\Element;

class MenuItem extends Element
{

    protected $item_name;

    protected $href;

    protected $selected;

    private $classLink = [];

    public function __construct(string $name, $item_name, $href, $selected = false, $active = false)
    {
        parent::__construct($name);
        $this->item_name = $item_name;
        $this->href = $href;
        $this->addClass('nav-item');
        $this->selected = (bool)$selected;

        $this->addClassLink('nav-link');
        if($active){
            $this->addClassLink('active');
        }
    }

    public function render(): string
    {
        return "<li class='{$this->getClass()}' role='presentation'>
                    <a id='{$this->getId()}' class='{$this->getClassLink()}' href='#{$this->href}' data-toggle='pill' role='tab' aria-controls='{$this->href}' aria-selected='{$this->selected}'>{$this->item_name}</a>
                </li>";
    }

    private function getClassLink()
    {
        return implode(' ', $this->classLink);
    }

    private function addClassLink($class){

        if(is_array($class)){
            foreach ($class as $cl){
                if(is_array($cl)){
                    $this->addClass($cl);
                }else{
                    if(!in_array($cl, $this->classLink)){
                        array_push($this->classLink, $cl);
                    }
                }
            }
            return $this;
        }

        if(!in_array($class, $this->classLink)){
            array_push($this->classLink, $class);
        }

        return $this;
    }

}