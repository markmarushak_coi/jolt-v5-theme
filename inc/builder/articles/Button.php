<?php

namespace inc\builder\articles;

use inc\builder\Element;

class Button extends Element
{

    private $url;

    private $text;

    private $options = [];

    public function __construct(string $name, string $url, string $text = '')
    {
        parent::__construct($name);
        $this->url = $url;
        $this->text = $text;
        $this->addClass('btn');
        $this->addClass('btn-custom');
        $this->addClass('btn-green');
    }

    public function render(): string
    {
        return "<a href='{$this->url}' class='{$this->getClass()}' {$this->getOptions()} >{$this->text}</a>";
    }

    public function addOption($name, $value)
    {
        if(!in_array($name, $this->options)){
            $this->options[$name] = $value;
        }
    }

    private function getOptions(): string
    {
        $output = "";
        foreach ($this->options as $name => $option){
            $output = " $name='$option' ";
        }
        return $output;
    }



}