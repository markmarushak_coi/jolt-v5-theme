<?php

namespace inc\builder\articles;

use inc\builder\Element;

class TitleName extends Element
{

    private $text;

    private $tag = "h3";

    public function __construct(string $name, string $text)
    {
        parent::__construct($name);
        $this->text = $text;
        $this->addClass('article__title__name order-1');
    }

    public function render(): string
    {
        return "<{$this->tag} class='{$this->getClass()}'>{$this->text}</{$this->tag}>";
    }

    public function defineTag($tag)
    {
        $list = [];

        if (is_string($tag)) {
            array_push($list, $tag);
        }

        if (is_array($tag)) {
            $list = $tag;
        }

        foreach ($list as $tag) {
            if (in_array($tag, ['h1', 'h2', 'h4', 'h5'], true)) {
                $this->tag = $tag;
            }
        }
    }
}