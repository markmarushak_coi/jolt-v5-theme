<?php

namespace inc\builder\articles;

use inc\builder\AbstractFactory;
use inc\builder\column\Column;
use inc\builder\Composite;
use inc\builder\row\Row;

class ArticleFactory extends AbstractFactory
{

    private $title = null;

    private $sub_title = null;

    private $image = null;

    private $body = null;

    private $button = null;

    private $option_button = [];

    public function __construct($data, Composite $container)
    {
        parent::__construct($data, $container);

        if(!empty($data)){
            $this->grid($data);
            $this->build($data);
        }
    }

    public function reset()
    {
        $this->title = null;
        $this->sub_title = null;
        $this->image = null;
        $this->body = null;
        $this->button = null;
        $this->option_button = [];
    }

    public function fill($article)
    {
        $this->image = $article['image'];
        $this->body = $article['body'];
        $this->title = $article['title'];
        $this->sub_title = $article['sub_title'];

        if(!empty($article['button']['text']) && $article['button']['link']){

            $this->button = array_filter($article['button'], function ($item){
               return !empty($item);
            });

            if($article['button']['add_option']){
                $this->option_button = $article['button']['options'];

            }
        }
    }


    public function build($articles)
    {
        $row = $this->createRow();



        foreach ($articles as $key => $article) {

            $col = new Column(generateRandomString());

            $col->addClass(['mb-3']);

            if($this->hasClass('grid')){
                $col->addClass($this->getClass('grid'));
            }

            $this->fill($article);

            $_article = $this->collect();

            $col->add($_article);

            $row->add($col);
        }

        $this->container->add($row);
    }

    protected function collect()
    {
        $article = new Article('article'); // container

        if(!empty($this->getClass('background'))){
            $article->addClass($this->getClass('background'));
        }

        if(!empty($this->getClass('article'))){
            $article->addClass($this->getClass('article'));
        }

        if(!empty($this->title)){

            $title = new Title('title'); // container

            if(!empty($this->getClass('title'))){
                $title->addClass($this->getClass('title'));
            }

            $title_name = new TitleName('name', $this->title); // element

            if(!empty($this->getClass('name'))){
                $title_name->addClass($this->getClass('name'));
                $title_name->defineTag($this->getClass('name', true));
            }


            $title->add($title_name);

            if(!empty($this->sub_title)) {
                $sub_title = new TitleSubName('sub_name', $this->sub_title); // element

                if(!empty($this->getClass('sub_name'))){
                    $sub_title->addClass($this->getClass('sub_name'));
                }

                $title->add($sub_title);
            }

            $article->add($title);
        }



        if(!empty($this->body)){
            $body = new Body('body', $this->body);

            if(!empty($this->getClass('body'))){
                $body->addClass($this->getClass('body'));
            }

            $article->add($body);
        }

        if(!empty($this->image)){
            $image = new Image('image', $this->image, $this->title);

            if(!empty($this->getClass('image'))){
                $image->addClass($this->getClass('image'));
            }

            if(!empty($this->getClass('img'))){
                $image->addClassImg($this->getClass('img'));
            }

            $article->add($image);
        }

        if(!empty($this->button)){
            $group_button = new GroupButton('group-button');

            if(!empty($this->getClass('group-button'))){
                $group_button->addClass($this->getClass('group-button'));
            }

            $button = new Button('button', $this->button['link'], $this->button['text']);

            if(!empty($this->getClass('button'))){
                $button->addClass($this->getClass('button'));
            }

            if(!empty($this->option_button)){
                foreach ($this->option_button as $option){
                    $button->addOption($option['name'], $option['value']);
                }
            }

            $group_button->add($button);

            $article->add($group_button);
        }


        return $article;
    }

}