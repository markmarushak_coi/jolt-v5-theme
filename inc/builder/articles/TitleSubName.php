<?php

namespace inc\builder\articles;

use inc\builder\Element;

class TitleSubName extends Element
{
    private $text;

    public function __construct(string $name, $text, $order = 2)
    {
        parent::__construct($name);
        $this->text = $text;
        $this->addClass('article__title__sub');
        $this->addClass('order-'.$order);
    }

    public function render(): string
    {
        return "<h3 class='{$this->getClass()}'>{$this->text}</h3>";
    }
}