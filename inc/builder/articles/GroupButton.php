<?php

namespace inc\builder\articles;

use inc\builder\Composite;

class GroupButton extends Composite
{
    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('article__button order-4');
    }

    public function render(): string
    {
        $output = parent::render();
        return "<div class='{$this->getClass()}'>$output</div>";
    }

}