<?php

namespace inc\builder\articles;

use inc\builder\Composite;

class Article extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('article__block');
    }

    public function render(): string
    {
        $output = parent::render();
        return "<div id='{$this->getId()}' class='{$this->getClass()}'>$output</div>";
    }

}