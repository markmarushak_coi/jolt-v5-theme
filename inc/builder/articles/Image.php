<?php

namespace inc\builder\articles;

use inc\builder\Element;

class Image extends Element
{

    private $url;

    private $alt;

    private $class = [];

    public function __construct(string $name, string $url, string $alt = '')
    {
        parent::__construct($name);
        $this->url = $url;
        $this->alt = $alt;
        $this->addClass('article__img order-1');
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'>
                    <img class='img {$this->getClassImg()}' src='{$this->url}' alt='{$this->alt}' loading='lazy'>
                </div>";
    }

    public function addClassImg($class){

        if(is_array($class)){
            foreach ($class as $cl){
                if(is_array($cl)){
                    $this->addClass($cl);
                }else{
                    if(!in_array($cl, $this->class)){
                        array_push($this->class, $cl);
                    }
                }
            }
            return $this;
        }

        if(!in_array($class, $this->class)){
            array_push($this->class, $class);
        }

        return $this;
    }

    public function removeClassImg($class)
    {
        if(is_array($class)){
            foreach ($class as $cl){
                if(in_array($cl, $this->class)){
                    $this->class = array_diff($this->class, [$cl]);
                }
            }
            return;
        }

        if(in_array($class, $this->class)){
            $this->class = array_diff($this->class, [$class]);
        }
    }

    public function getClassImg()
    {
        return implode(' ', $this->class);
    }

}