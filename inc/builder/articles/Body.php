<?php

namespace inc\builder\articles;

use inc\builder\Element;

class Body extends Element
{

    private $html;

    public function __construct(string $name, string $html)
    {
        parent::__construct($name);
        $this->html = $html;
        $this->addClass('article__body order-3');
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'>
                            {$this->html}
                        </div>";
    }

}