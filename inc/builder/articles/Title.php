<?php

namespace inc\builder\articles;

use inc\builder\Composite;

class Title extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('article__title order-2');
    }

    public function render(): string
    {
        $output = parent::render();

        return "<header class='{$this->getClass()}'>
                            $output
                        </header>";

    }

}