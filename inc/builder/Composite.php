<?php

namespace inc\builder;

abstract class Composite extends Element
{

    protected $fields = [];

    public function add(Element $field)
    {
        $name = $field->getName();
        $field->setParent($this);
        $this->fields[$name] = $field;
    }

    public function remove(Element $component): void
    {
        $this->fields = array_filter($this->fields, function ($child) use ($component) {
           return $child != $component;
        });
    }

    public function setData($data): void
    {
        foreach ($this->fields as $name => $field){
            if(isset($data[$name])){
                $field->setData($data[$name]);
            }
        }
    }

    public function getData(): array
    {
        $data = [];

        foreach ($this->fields as $name => $field) {
            $data[$name] = $field->getData();
        }

        return $data;
    }

    public function render(): string
    {

//        $this->preRender();

        $output = "";

        foreach ($this->fields as $name => $field) {
            $output .= $field->render();
        }

        return $output;
    }

    private function preRender()
    {
        foreach ($this->fields as $name => $field) {
            if(!empty($field->getToParent())){
                $field->getParent()->add($field->getToParent());
            }
        }
    }

    public function sortFields($flag = null)
    {
        if (isset($flag)){
            ksort($this->fields, $flag);
        }
        ksort($this->fields);
    }

}