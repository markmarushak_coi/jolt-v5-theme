<?php

namespace inc\builder\reviewSlider;

use inc\builder\AbstractFactory;
use inc\builder\Composite;
use inc\builder\reviews\ReviewAuthor;
use inc\builder\reviews\ReviewMessage;
use inc\builder\reviews\ReviewRating;
use inc\builder\reviews\ReviewTitle;
use inc\builder\reviews\ReviewWidget;
use inc\builder\row\Row;

class ReviewSliderFactory extends AbstractFactory
{

    private $message;

    private $author;

    private $rating;

    private $image;

    private $link;

    private $title;

    private $order;

    public function __construct($data, Composite $container)
    {
        parent::__construct($data, $container);

        if(!empty($data)){
            $this->build($data);
        }
    }

    public function reset()
    {
        $this->author = null;
        $this->message = null;
        $this->rating = null;
        $this->image = null;
        $this->link = null;
        $this->title = null;
        $this->order = "0";
    }

    public function fill($review)
    {
        $this->setAuthor($review['author']);
        $this->title = $review['title'];
        $this->message = $review['message'];
        $this->rating = $review['rating'];
        $this->image = $review['widget']['image'];
        $this->link = $review['widget']['link'];
        $this->order = $review['order'];
    }

    protected function build($reviews)
    {
        $row = $this->createRow();
        $row->removeClass('justify-content-center');
        $row->addClass([
            'align-items-lg-center',
            'position-relative'
        ]);

        $review_slider = new ReviewSlider(generateRandomString());
        $row->add($review_slider);

        $review_slider->add(new SliderControl(generateRandomString()));

        $slider_list = new SliderList(generateRandomString());
        $review_slider->add($slider_list);

        foreach ($reviews as $key => $review){
            $this->reset();
            $this->fill($review);

            $item = $this->collect();

            $slider_list->add($item);
        }

        $slider_list->sortFields();

        $this->container->add($row);
    }

    protected function collect()
    {
        $item = new SliderItem($this->order);

        $header = new ItemHeader(generateRandomString());

        if(!empty($this->rating)){
            $rating = new ReviewRating(generateRandomString(), $this->rating);
            $rating->addClass('review__rating-center');
            $rating->addClassStar('star star-xl');
            $header->add($rating);
        }

        $body = new ItemBody(generateRandomString());

        if(!empty($this->author)){
            $body->add(new ReviewAuthor(generateRandomString(), $this->author));
        }

        if(!empty($this->title)){
            $body->add(new ReviewTitle(generateRandomString(), $this->title));
        }

        if(!empty($this->message)){
            $body->add(new ReviewMessage(generateRandomString(), $this->message));
        }

        $footer = new ItemFooter(generateRandomString());

        if(!empty($this->image)){
            $footer->add(new ReviewWidget(generateRandomString(), $this->link, $this->image));
        }

        $item->add($header);
        $item->add($body);
        $item->add($footer);

        return $item;
    }

    private function setAuthor(string $author)
    {
        $this->author = "Reviewed by:&nbsp; <span class='author'>$author</span>";
    }

}