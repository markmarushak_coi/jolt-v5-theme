<?php

namespace inc\builder\reviewSlider;

use inc\builder\Composite;

class ItemHeader extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('item__header');
    }

    public function render(): string
    {
        $output = parent::render();
        return "<div class='{$this->getClass()}'>$output</div>";
    }

}