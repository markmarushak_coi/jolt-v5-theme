<?php

namespace inc\builder\reviewSlider;

use inc\builder\Composite;

class ReviewSlider extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('review__slider');
    }

    public function render(): string
    {
        $output = parent::render();
        /**
         * The function init_review_slider located in assets/js/review_slider/main.js
         *
         * imported in assets/js/custom.js
         */
        return "<div class='{$this->getClass()}'>$output</div>";
    }

}