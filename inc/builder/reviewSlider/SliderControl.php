<?php

namespace inc\builder\reviewSlider;

use inc\builder\Element;

class SliderControl extends Element
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('slider__control');
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'>
                        <div class='control__left'>
                            <img src='{$this->getPathGift("assets/img/chevron-down-green.svg")}' alt='review__control__left' loading='lazy'>
                        </div>
                        <div class='control__right'>
                            <img src='{$this->getPathGift("/assets/img/chevron-down-green.svg")}' alt='review__control__right' loading='lazy'>
                        </div>
                    </div>";
    }

    private function getPathGift(string $path): string
    {
        if($path[0] !== '/'){
            $path = '/' . $path;
        }

        return get_template_directory_uri() . $path;
    }
}