<?php

namespace inc\builder\reviewSlider;

use inc\builder\Composite;

class SliderItem extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('slider__item');
    }

    public function render(): string
    {
        $output = parent::render();
        return "<div class='{$this->getClass()}'>$output</div>";
    }

}