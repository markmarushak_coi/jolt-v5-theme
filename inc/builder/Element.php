<?php

namespace inc\builder;

abstract class Element
{

    protected $name;

    protected $data = [];

    private $class = [];

    protected $id = "";

    protected $additional = [];

    protected $parent;

    protected $to_parent = null;

    private static $instance = [];

    public function __construct(string $name)
    {
        $this->construct($name);
    }

    private function construct($name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setData($data): void
    {
        array_push($this->data, $data);
    }

    public function getData()
    {
        $output = '';

        foreach ($this->data as $el){
            $output .= $el;
        }

        return $output;
    }

    public function addClass($class){

        if(is_array($class)){
            foreach ($class as $cl){
                if(is_array($cl)){
                    $this->addClass($cl);
                }else{
                    if(!in_array($cl, $this->class)){
                        array_push($this->class, $cl);
                    }
                }
            }
            return $this;
        }

        if(!in_array($class, $this->class)){
            array_push($this->class, $class);
        }

        return $this;
    }

    public function removeClass($class)
    {
        if(is_array($class)){
            foreach ($class as $cl){
                if(in_array($cl, $this->class)){
                    $this->class = array_diff($this->class, [$cl]);
                }
            }
            return;
        }

        if(in_array($class, $this->class)){
            $this->class = array_diff($this->class, [$class]);
        }
    }

    public function getClass()
    {
        return implode(' ', $this->class);
    }

    abstract public function render(): string;

    /**
     * @param $name
     * @param null $class
     * @return Element
     */
    public static function create($name, $class = null)
    {
        if($class){
            self::$instance[$name] = $class;
        }else{
            self::$instance[$name] = new static($name);
        }

        return self::$instance[$name];
    }

    /**
     * @param $name
     * @return mixed|null
     */
    public static function get($name)
    {
        if(isset(self::$instance[$name])){
            return self::$instance[$name];
        }

        return null;
    }

    /**
     * @return Composite
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param $parent
     */
    public function setParent(Composite $parent)
    {
        $this->parent = $parent;
    }

    public function getToParent()
    {
        return $this->to_parent;
    }

    public function setToParent(Element $element)
    {
        $this->to_parent = $element;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return Element
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return array
     */
    public function getAdditional(): string
    {
        return implode(' ',$this->additional);
    }

    /**
     * @param array $additional
     */
    public function setAdditional($additional)
    {
        $this->additional[] = $additional;
    }



}