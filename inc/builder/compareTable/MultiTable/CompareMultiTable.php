<?php


namespace inc\builder\compareTable\MultiTable;


use inc\builder\Composite;

class CompareMultiTable extends Composite
{
    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('table__multi_compare');
    }

    public function render(): string
    {
        $output = parent::render();
        return "<div id='{$this->getId()}' class='{$this->getClass()}'>$output</div>";
    }
}