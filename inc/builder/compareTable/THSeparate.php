<?php

namespace inc\builder\compareTable;

use inc\builder\Element;

class THSeparate extends Element
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('table__separate');
    }

    public function render(): string
    {
        return "<th class='{$this->getClass()}'></th>";
    }

}