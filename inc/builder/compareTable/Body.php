<?php

namespace inc\builder\compareTable;

use inc\builder\Composite;

class Body extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('table__body');
    }

    public function render(): string
    {
        $output = parent::render();
        return "<tr class='{$this->getClass()}'>$output</tr>";
    }

}