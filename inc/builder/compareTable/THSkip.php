<?php

namespace inc\builder\compareTable;

use inc\builder\Element;

class THSkip extends Element
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('head__skip');
    }

    public function render(): string
    {
        return "<th class='{$this->getClass()}'></th>";
    }

}