<?php

namespace inc\builder\compareTable;

use inc\builder\Composite;
use inc\builder\Element;

class TDProduct extends Composite
{

    private $product_name;

    private $row;

    public function __construct(string $name, $product_name, $row = null)
    {
        parent::__construct($name);
        $this->addClass('table__product');
        $this->product_name = $product_name;
        $this->row = $row;
        $this->add(new TDSeparate(generateRandomString()));
    }

    public function render(): string
    {
        $output = parent::render();
        if($this->row == 'reviews'){
            return "<td class='{$this->getClass()}'><a href='{$this->product_name["link"]}'>{$this->product_name["text"]}</a></td> $output";
        }
        return "<td class='{$this->getClass()}'>{$this->product_name}</td> $output";
    }

}