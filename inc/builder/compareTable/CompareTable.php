<?php
/**
 * Created by PhpStorm.
 * User: Mark Marushchak
 * Date: 27.07.2020
 * Time: 14:42
 */

namespace inc\builder\compareTable;

use inc\builder\Composite;

class CompareTable extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('table__compare');
    }

    public function render(): string
    {
        $output = parent::render();
        return "<table class='{$this->getClass()}'>$output</table>";
    }

}