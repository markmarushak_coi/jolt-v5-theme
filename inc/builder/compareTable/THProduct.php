<?php

namespace inc\builder\compareTable;

use inc\builder\Composite;

class THProduct extends Composite
{

    private $product_name;

    public function __construct(string $name, $product_name)
    {
        parent::__construct($name);
        $this->addClass('table__product');
        $this->product_name = $product_name;
        $this->add(new THSeparate(generateRandomString()));
    }

    public function render(): string
    {
        $output = parent::render();
        return "<th class='{$this->getClass()}'>{$this->product_name}</th>$output";
    }

}