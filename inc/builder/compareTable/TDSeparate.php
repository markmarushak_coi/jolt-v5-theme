<?php

namespace inc\builder\compareTable;

use inc\builder\Element;

class TDSeparate extends Element
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('table__separate');
    }

    public function render(): string
    {
        return "<td class='{$this->getClass()}'></td>";
    }

}