<?php

namespace inc\builder\compareTable;

use inc\builder\Element;

class TDProductDescription extends Element
{

    private $parameter_name;

    public function __construct(string $name, $parameter_name)
    {
        parent::__construct($name);
        $this->addClass('body__description');
        $this->parameter_name = $parameter_name;
    }

    public function render(): string
    {
        return "<td class='{$this->getClass()}'><span>{$this->parameter_name}</span></td>";
    }

}