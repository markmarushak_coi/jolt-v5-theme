<?php

namespace inc\builder\compareTable;

use inc\builder\AbstractFactory;
use inc\builder\Composite;
use inc\builder\domainNames\Content;
use inc\builder\domainNames\Menu;
use inc\builder\domainNames\MenuItem;

class CompareTableFactory extends AbstractFactory
{

    // name of product
    private $name = null;

    private $thead = [];

    private $tbody = [];

    private $products = [];

    private $multi_tables = false;

    private $tables_name = [];


    public function __construct($data, Composite $container)
    {

        parent::__construct($data, $container);

        $this->filter($data);
    }

    public function reset()
    {
        $this->name = null;
        $this->thead = [];
        $this->tbody = [];
    }

    public function fill($product)
    {
        $post = $this->getPost($product);

        $this->name = $post->post_title;

        $post_data = json_decode($post->post_content);
        $this->thead = array_shift($post_data);

        $this->tbody = $post_data;
    }

    protected function build($products)
    {
        foreach ($products as $key => $product) {
            $this->reset();
            $this->fill($product);

        }

        $table = $this->collect();

        $table->addClass('order-2');

        $this->container->getParent()->add($table);

        $this->container->addClass('order-1');


    }

    private function getPost($data) : \WP_Post
    {
        return $data;
    }

    protected function collect()
    {
        $table = new CompareTable('compare_table');

        //build head
        $head = new Head(generateRandomString());

        foreach ($this->thead as $head_key => $name) {

            if(!$head_key){
                $head->add(new THSkip(generateRandomString()));
            }else{
                $head->add(new THProduct(generateRandomString(), $name));
            }
        }

        $table->add($head);

        // build body
        foreach ($this->tbody as $row) {
            $body = new Body(generateRandomString());

            $name = '';
            foreach ($row as $key_body => $td) {
                if(!$key_body){
                    $name = $td;
                    $body->add(new TDProductDescription(generateRandomString(), $td));
                }else{
                    $body->add(new TDProduct(generateRandomString(), $td, $name));
                }
            }

            $table->add($body);

        }


        return $table; // ready table
    }


    private function filter(&$data)
    {
        foreach ($data as $layout) {
            if ($layout['acf_fc_layout'] == 'products'){
                $this->products = $layout['products'];
            }else{
                $this->multi_tables = $layout['status'];
                $this->tables_name = $layout['tabs_name'];
            }
        }

        if ($this->multi_tables) {
           return $this->buildMulti();
        }

        return $this->build($this->products);
    }

    private function buildMulti()
    {
        $tab_menu = new Menu(generateRandomString(5));
        $tab_menu->setId($tab_menu->getName());
        $tab_menu->addClass('nav-tabs');

        $tab_content = new Content(generateRandomString());
        $tab_content->addClass('tab-content');
        $tab_content->setId($tab_content->getName());

        $tables = [];
        foreach ($this->products as $product){
            $this->reset();
            $this->fill($product);
            array_push($tables, $this->collect());
        }

        $active = true;

        foreach ($tables as $key => $table) {
            $uniq_id = generateRandomString();

            $item_name = $this->tables_name[$key]['name'];
            $item = new MenuItem(generateRandomString(), $item_name, 'content-'.$uniq_id, $active, $active);
            $item->setId('tab-'.$uniq_id);
            $tab_menu->add($item);

            $container = $this->createColumn();
            $container->addClass(['tab-pane', 'fade', 'table-responsive-xl']);
            if($active){
                $container->addClass(['active', 'show']);
            }

            $container->setId('content-'.$uniq_id);
            $container->setAdditional('role="tabpanel"');
            $container->setAdditional('aria-labelledby="tab-'.$uniq_id.'"');
            $container->add($table);
            $tab_content->add($container);

            $active = false;
        }

        $compareTable = $this->createColumn();
        $compareTable->addClass('order-2');
        $compareTable->addClass('table__multi_compare');
        $compareTable->add($tab_menu);
        $compareTable->add($tab_content);

        $this->container->getParent()->add($compareTable);

        $this->container->addClass('order-1');

    }


}