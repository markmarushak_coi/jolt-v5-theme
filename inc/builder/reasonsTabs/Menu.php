<?php

namespace inc\builder\reasonsTabs;

use inc\builder\Composite;

class Menu extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('reason__list');
    }

    public function render(): string
    {
        $output = parent::render();
        return "<div class='{$this->getClass()}'>$output</div>";
    }

}