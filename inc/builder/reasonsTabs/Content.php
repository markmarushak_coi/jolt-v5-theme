<?php

namespace inc\builder\reasonsTabs;

use inc\builder\Composite;

class Content extends Composite
{

    private $left = "<div class='left'><span class='chevron chevron-right-green'></span></div>";

    private $right = "<div class='right'><span class='chevron chevron-right-green'></span></div>";

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('reason__content');
    }

    public function render(): string
    {
        $output = parent::render();

        return "<div class='{$this->getClass()}'> {$this->getLeft()} $output {$this->getRight()}</div>";
    }

    /**
     * @return string
     */
    public function getLeft(): string
    {
        return $this->left;
    }

    /**
     * @param string $left
     */
    public function setLeft(string $left): void
    {
        $this->left = $left;
    }

    /**
     * @return string
     */
    public function getRight(): string
    {
        return $this->right;
    }

    /**
     * @param string $right
     */
    public function setRight(string $right): void
    {
        $this->right = $right;
    }

}