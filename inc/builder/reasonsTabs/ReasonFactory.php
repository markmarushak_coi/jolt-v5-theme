<?php

namespace inc\builder\reasonsTabs;

use inc\builder\AbstractFactory;
use inc\builder\articles\Article;
use inc\builder\articles\Body;
use inc\builder\articles\Button;
use inc\builder\articles\GroupButton;
use inc\builder\articles\Image;
use inc\builder\articles\Title;
use inc\builder\articles\TitleName;
use inc\builder\articles\TitleSubName;
use inc\builder\column\Column;
use inc\builder\Composite;
use inc\builder\row\Row;

class ReasonFactory extends AbstractFactory
{

    private $title;

    private $sub_title;

    private $image;

    private $body;

    private $button;

    private $tab;

    private $menu;

    private $contents;

    private $active = true;

    public function __construct($data, Composite $container)
    {
        parent::__construct($data, $container);
        $this->build($data);
    }

    public function reset()
    {
        $this->title = null;
        $this->sub_title = null;
        $this->image = null;
        $this->body = null;
        $this->tab = null;
        $this->button = null;
    }

    protected function fill($reason)
    {
        $this->tab = $reason['tab'];

        $content = $reason['content']['articles'][0];
        $this->image = $content['image'];
        $this->body = $content['body'];
        $this->title = $content['title'];
        $this->sub_title = $content['sub_title'];

        if(!empty($content['button'])){
            $this->button = $content['button'];
        }
    }

    protected function build($reasons)
    {
        $row = new Row(generateRandomString());
        $col = new Column(generateRandomString());
        $row->add($col);

        $this->setMenu(new Menu(generateRandomString()));
        $this->setContents(new Content(generateRandomString()));

        foreach ($reasons as $key => $reason){
            $this->reset();
            $this->fill($reason);
            $this->collect();
        }

        $reason = new Reason('reason');
        $reason->add($this->getMenu());
        $reason->add($this->getContents());

        $col->add($reason);

        $this->container->add($row);

    }

    protected function collect()
    {
        $key = generateRandomString();

        $item = new MenuItem(generateRandomString(), $this->tab, $key);
        $this->getMenu()->add($item);

        $article = new Article(generateRandomString());
        $article->setId($key);

        if(!empty($this->title)){

            $title = new Title('title'); // container

            if(is_string($this->getClass('title'))){
                $title->addClass($this->getClass('title'));
            }

            $title_name = new TitleName('name', $this->title);

            if(!empty($this->getClass('name'))){
                $title_name->addClass($this->getClass('name'));
            }

            $title->add($title_name);

            if(!empty($this->sub_title)) {
                $sub_title = new TitleSubName('sub_name', $this->sub_title); // element

                if(!empty($this->getClass('sub_name'))){
                    $sub_title->addClass($this->getClass('sub_name'));
                }

                $title->add($sub_title);
            }

            $article->add($title);
        }



        if(!empty($this->body)){
            $body = new Body('body', $this->body);

            if(!empty($this->getClass('body'))){
                $body->addClass($this->getClass('body'));
            }

            $article->add($body);
        }

        if(!empty($this->image)){
            $image = new Image('image', $this->image, $this->title);

            if(!empty($this->getClass('image'))){
                $image->addClass($this->getClass('image'));
            }

            $article->add($image);
        }

        if(!empty($this->button['text'])){
            $group_button = new GroupButton('group-button');

            if(!empty($this->getClass('group-button'))){
                $group_button->addClass($this->getClass('group-button'));
            }

            $button = new Button('button', $this->button['link'], $this->button['text']);

            if(!empty($this->getClass('button'))){
                $button->addClass($this->getClass('button'));
            }

            $group_button->add($button);

            $article->add($group_button);
        }

        $this->getContents()->add($article);

        if($this->active){
            $item->addClass('active');
            $article->addClass('active');
            $this->active = false;
        }


    }

    /**
     * @return Composite
     */
    public function getMenu(): Composite
    {
        return $this->menu;
    }

    /**
     * @param Composite $menu
     */
    public function setMenu(Composite $menu)
    {
        $this->menu = $menu;
    }

    /**
     * @return Composite
     */
    public function getContents(): Composite
    {
        return $this->contents;
    }

    /**
     * @param Composite $contents
     */
    public function setContents(Composite $contents)
    {
        $this->contents = $contents;
    }



}