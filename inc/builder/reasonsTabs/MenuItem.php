<?php

namespace inc\builder\reasonsTabs;

use inc\builder\Element;

class MenuItem extends Element
{

    protected $item_name;

    protected $data_key;

    public function __construct(string $name, $item_name, $data_key)
    {
        parent::__construct($name);
        $this->item_name = $item_name;
        $this->data_key = $data_key;
        $this->addClass('reason__item');
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}' data-key='{$this->data_key}'>
                                <span class='reason__item__text'>{$this->item_name}</span>
                                <span class='chevron'></span>
                            </div>";
    }

}