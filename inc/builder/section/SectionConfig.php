<?php

namespace inc\builder\section;

use inc\builder\Data;

class SectionConfig extends Data
{
    protected static $instance = [];

    public static function instance($id)
    {
        if (in_array($id, self::$instance)) {
            return self::$instance[$id];
        }

        self::$instance[$id] = new Data();

        return self::$instance[$id];
    }

}