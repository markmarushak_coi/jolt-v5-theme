<?php

namespace inc\builder\section;

use inc\builder\AbstractFactory;
use inc\builder\background\Background;
use inc\builder\background\Substrate;
use inc\builder\Composite;
use inc\builder\container\Container;
use inc\builder\TemplateFactory;
use inc\builder\section\Section as TemplateSection;

class SectionFactory extends AbstractFactory
{

    private $block = null;

    private $block_type = null;

    /**
     * SectionFactory constructor.
     * @param array $data
     * @param Section $section
     * @return Composite
     */
    public function __construct(array $data, TemplateSection $section)
    {
        parent::__construct($data, $section);

        $this->background();

        $this->substrate();

        $this->build($data);

        return $this->container;
    }

    protected function fill($data)
    {
        $this->block_type = $data['acf_fc_layout'];
        $this->block = $data[$this->block_type];
    }

    protected function reset()
    {
        $this->block = null;
        $this->block_type = null;
    }

    protected function build($data)
    {

        if($this->hasClass('section')){
            $this->container->addClass($this->getClass('section'));
        }

        if($this->hasClass('id')){
            $this->container->setId($this->getClass('id'));
        }

        $container = Container::create('container');

        $container->setParent($this->container);

        if($this->hasClass('container')){
            $container->addClass($this->getClass('container'));
        }

        foreach ($data as $block){

            $this->reset();

            $this->fill($block);

            $this->collect($container);

        }


        $this->container->add($container::get('container'));
    }

    /**
     * @param null $container
     * @return Composite|void
     */
    protected function collect($container = null)
    {

        $template = new TemplateFactory($this->block, $this->block_type, $container);
        $template->create();

    }

    public function render()
    {
        return $this->container->render();
    }

    private function background()
    {
        if($this->hasClass('background')){
            $background = new Background('background', $this->getClass('background'));

            $this->container->add($background);
        }
    }

    private function substrate()
    {
        if($this->hasClass('substrate')){
//            $background = new Substrate('substrate', $this->getClass('substrate'));

//            $this->container->add($background);
        }
    }


}