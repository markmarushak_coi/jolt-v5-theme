<?php

namespace inc\builder\section;

use inc\builder\Composite;

class Section extends Composite
{

    protected $content = [];

    protected $modification = [];

    protected $background = null;

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('custom__section');
    }

    public function render(): string
    {
        $output = parent::render();
        return "<section id='{$this->getId()}' class='{$this->getClass()}'>$output</section>";
    }

}