<?php

namespace inc\builder;

use inc\builder\banner\BannerFactory;
use inc\builder\compareTable\CompareTableFactory;
use inc\builder\domainNames\DomainNameFactory;
use inc\builder\faqs\FAQsFactory;
use inc\builder\product\ProductFactory;
use inc\builder\articles\ArticleFactory;
use inc\builder\reasonsTabs\ReasonFactory;
use inc\builder\reviews\ReviewFactory;
use inc\builder\reviewSlider\ReviewSliderFactory;

class TemplateFactory
{

    private $template;

    private $data;

    private $section;

    private $templates = [
        'articles' => ArticleFactory::class,
        'products' => ProductFactory::class,
        'reviews' => ReviewFactory::class,
        'review_slider' => ReviewSliderFactory::class,
        'compare' => CompareTableFactory::class,
        'banner' => BannerFactory::class,
        'faqs' => FAQsFactory::class,
        'reasons' => ReasonFactory::class,
        'domain_names' => DomainNameFactory::class,
    ];

    public function __construct(array $data, $template, Composite $section)
    {
        $this->template = $template;
        $this->setData($data);
        $this->section = $section;
    }

    public function create()
    {
        if(empty($this->templates[$this->template])){
            return new \WP_Error( 'template_factory',"template $this->template not found");
        }

        $factory = $this->templates[$this->template];
        return new $factory($this->data->getData(), $this->section);
    }

    public function setData($data)
    {
        if(!empty($data)){
            $this->data = new Data($data);
        }
    }

}