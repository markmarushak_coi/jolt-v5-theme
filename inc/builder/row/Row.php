<?php

namespace inc\builder\row;

use inc\builder\Composite;

class Row extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('row');
        $this->addClass('justify-content-center');
    }

    public function render(): string
    {
        $output = parent::render();
        return "<div class='{$this->getClass()}'>$output</div>";
    }

}