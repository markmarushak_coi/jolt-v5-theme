<?php

namespace inc\builder;

use inc\builder\column\Column;
use inc\builder\row\Row;

abstract class AbstractFactory
{
    protected $html = '';

    protected $container;

    protected $modifications = [];

    public function __construct(array &$data, Composite $container)
    {

        $this->reset();

        $this->container = $container;

        foreach ($data as $key => $item){

            if(gettype($item) != 'array'){
                continue;
            }

            if(isset($item['acf_fc_layout']) && strpos($item['acf_fc_layout'], 'modifications') !== false){

                foreach ($item[$item['acf_fc_layout']] as $modification){

                    $name =  $modification['key'];
                    if(!empty($modification[$name])){
                        $value = $modification[$name];
                    }else{
                        continue;
                    }


                    if(!$this->hasClass($name)){
                        $this->modifications[$name] = [];
                    }

                    if(is_array($value)){
                         array_filter($value, function($item) use ($name){
                             if(!empty($item)){
                                 array_push($this->modifications[$name], $item);
                             }
                        });
                    }else{
                        array_push($this->modifications[$name], $value);
                    }
                }

                unset($data[$key]);
            }
        }

    }

    public function addHtml($text)
    {
        $this->html .= $text;
    }

    public function getHtml()
    {
        return $this->html;
    }

    /**
     * The function return list of class of tag
     * @param $name
     * @param bool $return_array
     * @return string
     */
    public function getClass($name, $return_array = false)
    {
        if(empty($this->modifications[$name])){
            return null;
        }

        if ($return_array) {
            return $this->modifications[$name];
        }

        return implode(' ', $this->modifications[$name]);
    }

    public function hasClass($name, $container = 'modifications')
    {
        if(isset($this->{$container}) && isset($this->{$container}[$name])){
            return true;
        }

        return false;
    }

    /**
     * The function fill base parameters
     * @param $data
     * @return mixed
     */
    abstract protected function fill($data);

    /**
     * The function reset base parameters
     */
     protected function reset(){}

    /**
     * The function works like a controller that builds the main blocks in a section
     * @param array $data
     * @return mixed
     */
    abstract protected function build(array $data);

    /**
     * The function collects the base
     * @return Composite
     */
    abstract protected function collect();

    protected function grid($data)
    {
        if(!$this->hasClass('grid')){
            $grid = round(12 / count($data));

            $grid = 'col-md-' . $grid;

            $this->modifications['grid'] = [];

            array_push($this->modifications['grid'], $grid);
        }
    }

    protected function createColumn()
    {
        return new Column(generateRandomString());
    }

    protected function createRow()
    {
        $row = new Row(generateRandomString());

        if(!empty($this->getClass('row'))){
            $row->addClass($this->getClass('row'));
        }
        
        return $row;
    }
}