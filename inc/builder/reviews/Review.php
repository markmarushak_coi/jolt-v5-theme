<?php

namespace inc\builder\reviews;

use inc\builder\Composite;

class Review extends Composite
{

    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->addClass('review__block');
    }

    public function render(): string
    {
        $output = parent::render();
        return "<div class='{$this->getClass()}'>$output</div>";
    }

}