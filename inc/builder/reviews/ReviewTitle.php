<?php

namespace inc\builder\reviews;

use inc\builder\Element;

class ReviewTitle extends Element
{

    private $title;

    public function __construct(string $name, $title)
    {
        parent::__construct($name);
        $this->title = $title;
        $this->addClass('review__title');
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'>{$this->title}</div>";
    }

}