<?php

namespace inc\builder\reviews;

use inc\builder\Element;

class ReviewWidget extends Element
{

    private $link;

    private $image;

    public function __construct(string $name, $link, $image)
    {
        parent::__construct($name);
        $this->image = $image;
        $this->link = $link;
        $this->addClass('widget');
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'>
                    Reviewed on:
                    <div class='widget__code'>
                        <a href='{$this->link}' target='_blank'>
                            <img src='{$this->image}' alt='review center' loading='lazy'>
                        </a>
                    </div>
                </div>";
    }

}