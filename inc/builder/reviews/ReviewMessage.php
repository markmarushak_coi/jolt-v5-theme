<?php

namespace inc\builder\reviews;

use inc\builder\Element;

class ReviewMessage extends Element
{

    private $message;

    public function __construct(string $name, $message)
    {
        parent::__construct($name);
        $this->message = $message;
        $this->addClass('review__message');
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'>{$this->message}</div>";
    }

}