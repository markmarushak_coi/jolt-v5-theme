<?php

namespace inc\builder\reviews;

use inc\builder\Element;

class ReviewRating extends Element
{

    private $stars = "<span class='%s'></span>";

    private $stars_class = [];

    public $count;

    public function __construct(string $name, $count = 5)
    {
        parent::__construct($name);
        $this->count = $count;
        $this->addClass('review__rating');
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'>{$this->getStars()}</div>";
    }

    /**
     * @return string
     */
    public function getStars()
    {
        $output = '';
        for ($i = 0; $i < $this->count; $i++){
            $star = $this->stars;

            if($this->hasClassStar()){
                $star = sprintf ($star, $this->getClassStar());
            }

            $output .= $star;
        }

        return $output;
    }

    /**
     * @param string $stars
     */
    public function setStars(string $stars)
    {
        $this->stars = $stars;
    }

    public function addClassStar(string $class)
    {
        array_push($this->stars_class, $class);

        return $this;
    }

    public function getClassStar()
    {
        return implode(' ', $this->stars_class);
    }

    public function hasClassStar(): bool
    {
        return count($this->stars_class) ?? false;
    }

}