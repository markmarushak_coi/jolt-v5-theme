<?php

namespace inc\builder\reviews;

use inc\builder\AbstractFactory;
use inc\builder\background\Background;
use inc\builder\column\Column;
use inc\builder\Composite;
use inc\builder\row\Row;

class ReviewFactory extends AbstractFactory
{

    private $message = null;

    private $author = null;

    private $rating = null;

    private $image = null;

    public function __construct($data, Composite $container)
    {
        parent::__construct($data, $container);
        $this->build($data);
    }

    public function reset()
    {
        $this->author = null;
        $this->message = null;
        $this->rating = null;
    }

    public function fill($review)
    {
        $this->message = $review['message'];
        $this->author = $review['author'];
        $this->rating = $review['rating'];
    }

    protected function build($reviews)
    {

        $row = $this->createRow();
        $row->removeClass('justify-content-center');
        $row->addClass([
            'align-items-lg-center',
            'position-relative'
        ]);

        $background = new Background('background', 'background_review background_flex-center d-lg-flex d-none');
        $row->add($background);

        foreach ($reviews as $key => $review){

            $col = new Column(generateRandomString());

            if($key == 1){
                $col->addClass('col-lg-7 col-12 offset-lg-5');
            }else{
                $col->addClass('col-lg-7 col-12 offset-lg-4');
            }

            $this->reset();
            $this->fill($review);
            $review = $this->collect();

            $col->add($review);
            $row->add($col);

        }

        $this->container->add($row);

    }

    protected function collect()
    {
        $review = new Review(generateRandomString());


        $rating = new ReviewRating(generateRandomString(), $this->rating);
        $rating->addClassStar('star');

        $review->add($rating);

        $review->add(new ReviewMessage(generateRandomString(), $this->message));
        $review->add(new ReviewAuthor(generateRandomString(), $this->author));

        return $review;
    }

}