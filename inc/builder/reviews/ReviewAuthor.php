<?php

namespace inc\builder\reviews;

use inc\builder\Element;

class ReviewAuthor extends Element
{

    private $author;

    public function __construct(string $name, $author)
    {
        parent::__construct($name);
        $this->author = $author;
        $this->addClass('review__author');
    }

    public function render(): string
    {
        return "<div class='{$this->getClass()}'>{$this->author}</div>";
    }

}