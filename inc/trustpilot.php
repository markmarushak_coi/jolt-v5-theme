<div class="container">
	<div class="trust_block">
		<?php

		if(get_field('block_5_servers')){
			$trust_cat = get_field( "trust_pilot_categories" );
		} else{
			$trust_cat = array(14, 15, 16, 17);
		}
        $args = array(
        	'post_type' => 'trustpilot',
            'posts_per_page' => 1, 
            'order' => 'desc',
            'orderby' => 'rand',

            'tax_query' => array(
				'relation' => 'AND',
				array(
				'taxonomy' => 'trustpilot_cat',
				'field' => 'id',
				'terms' => $trust_cat,
				'include_children' => false,
				'operator' => 'IN'
				)
			), 

        );
        $wp_query = new WP_Query($args);
        ?>
        <?php if( $wp_query->have_posts() ): ?>
        	<ul>
            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
	            <li>
		            <p><?php the_content();?></p>
					<p class="reviewer"><?php the_field('reviewer_name');?></p>
	            </li>
            <?php endwhile;?>
        	</ul>

        <?php endif;?>
        <?php wp_reset_postdata(); wp_reset_query();?>
    </div>
</div>