<h1>Account Cancellation Request</h1>
<p>As much as we hate to see you leave us, we would highly appreciate if you could complete this small form for us. It will give us all the necessary information such as your order details, consent to cancel service and most importantly reasons of why you have made this decision. We will use this information to improve our service further.</p>	
<!-- Begin Cancellation form code -->
<?php
/*------ Some general Variables to customize ------------------*/

// Support Team Email Address - this is the address to receive notices at
$supportemail       =   "billing_accounting@webhostingbuzz.com";

// Subject of the cancellation email sent to the support team
$subjectemail       =   "Account Cancellation Request";
// Message of the cancellation email sent to the support team - remember \n acts as a break line and do not rename nor remove the variables
$messageemail       = "**** New Cancellation Request **** \n
Domain Name: ".$_POST['domain']." \n
Username: ".$_POST['username']."\n
Order Number: ".$_POST['ordernum']."\n
Client Name: ".$_POST['fullname']."\n
Client Address: ".$_POST['address']."\n
Phone Number: ".$_POST['phone']."\n
Email Address: ".$_POST['email']."\n
Hosting Plan: ".$_POST['plan_type']."\n
Has been a client for: ".$_POST['duration']."\n
Happy Overall? ".$_POST['overall_happy']."\n
Would recommend to friend? ".$_POST['recommend']."\n
Value for the money? ".$_POST['value']."\n
How was initial setup? ".$_POST['init_setup']."\n
How reliable? ".$_POST['reliability']."\n
Tech Support Experience? ".$_POST['tech_exp']."\n
Easy to use control panel? ".$_POST['easy_admin']."\n
Selected Reason: ".$_POST['option']." \n
Additional Reasons: ".$_POST['reasons']." \n
When Cancel: ".$_POST['when_cancel']." \n ";;
// Headers of the cancellation email --------
/*
// Debug headers only
$headers = 'From: billing@webhostingbuzz.com' . "\r\n" .
'Reply-To: billing@webhostingbuzz.com' . "\r\n" .
'X-Mailer: PHP/' . phpversion();
*/
//Set a friendly from variable
$from = $_POST['email'];
$headers = "From: \"".$from."\" <".$from.">\n";
$headers .= "Reply-To: <".$from.">\n";


/*----- End of general Variables -----------------------------*/


// STEP 1 - present the client with some cancellation options

// You can add as many options as you want, just make sure to follow the current structure
// such as $STEP1_option_4 , $STEP1_option_5, etc...
// Also make sure to add that new variable in the array variable $var_optionlists

$STEP1_option_1     =   " No longer need hosting for this domain";

$STEP1_option_2     =   " Current plan offers are better than my existing plan";

$STEP1_option_3     =   " Technical support issues";

$STEP1_option_4     =   " I have moved to another host";

$STEP1_option_5     =   " Missing Features";

$STEP1_option_6     =   " I need a bigger plan!";

$STEP1_option_7     =   " Other reasons";

//Be sure to add above vars to the array of displayed options
$var_optionlists = array ($STEP1_option_1, $STEP1_option_2, $STEP1_option_3, $STEP1_option_4, $STEP1_option_5, $STEP1_option_6, $STEP1_option_7);


// STEP 2 - variables of all the different options
// Make sure the numbering of the solution follows the numbering of the option list... such as solution_1 is for option_1 and so on
// You can use html tags such as <br> <b> etc... the message can be as long as you want
// Can add as many as you'd like

$STEP2_solution_1   =   "Did you know that we can change your main domain name? Many of our clients enjoy keeping their existing package at their locked in low price, and simply change domains to one that hosting is needed for, or simply a fun domain to host.";

$STEP2_solution_2   =   "Did you know you may be eligible for a free upgrade to current quotas? Please contact our billing department via <a href='http://www.whbsupport.com'>our helpdesk</a>.";

$STEP2_solution_3   =   "Management wants to hear about this and resolve your issue immediately without question. Please send an email to management [at] webhostingbuzz.com so we can discuss this personally.";

$STEP2_solution_4   =   "Though we are sad to hear you have already moved your account, if you could kindly take a moment to let us know the reasons for your move on the next page, this will help us provide the best customer experience going forward.";

$STEP2_solution_5   =   "Often our customers are surprised to find that we can offer many custom solutions. Please send in a ticket with your needs to <a href='http://www.whbsupport.com'>http://www.whbsupport.com</a> and we will do our very best to accommodate your needs.";

$STEP2_solution_6   =   "Talk to us. What plan are you on? Not only do we provide great budget plans, but we also have the infrastructure to provide fully custom dedicated servers and clusters. Please kindly submit a ticket to <a href='http://www.whbsupport.com'>our helpdesk</a> and we'll be very happy to work with you.";

// when a solution is empty, it means the user goes directly to the cancellation form without being prompt a solution
$STEP2_solution_7   =   "";

// Form Starting --------------------------------------------------------

echo "<form id='step1' action='' method='post'>";

if ($_POST['option'] == '')
{
echo "<table cellpadding='0' cellspacing='0' class='ca-table'>";
	$id = 0;
	foreach($var_optionlists as $options)
	{
		$id ++;
		
		echo "<tr><td class='ca-table-radio'><input type='radio' value='".$options."-".$id."' name='option' /></td><td>".$options."</td></tr>";
	}

echo "</table>";
echo "<div class='submit-box'><input type='submit' name='choicebt' class='input-submit' value='Continue' /></div>";
}
else
{
	/*- not to be changed ----------------------------------------*/
	$option = explode("-", $_POST['option']);
	$temp = "STEP2_solution_".$option['1'];
	echo "<input type='hidden' name='option' value='".$option['0']."' />";
	/*------------------------------------------------------------*/
	
	echo "<p><span class='red'>Selected Reason:</span><br />".$option['0']."</p>";
	
	if (${$temp} == '' OR $_POST['action'] != "")
	{
		if ($_POST['action'] == 'cancel' AND ($_POST['email'] == '' OR $_POST['domain'] == ''))
		{
			echo "<p class='red'>Please fill out all fields</p>";           
		}
		
		if ($_POST['action'] == 'cancel' AND $_POST['email'] != '' AND $_POST['domain'] != '')
		{
			echo "<p><span class='bold'>Cancellation submitted:</span> Though we are sad to see you leave, thank you for taking the time to complete this form. An email has been sent to our staff and your request will be handled promptly.</p>";

			mail ($supportemail, $subjectemail, $messageemail, $headers);   
			
			$hide = 1;  
		}
		
		if ($hide != 1)
		{
			echo "
			
			<h3>Please provide us with the following to verify your account and process cancellation:</h3>
		
			<div class='ca-col1'>
			<label>Account main domain name:</label>
		
			<input type='text' class='input-text' name='domain' size='30' value=".$_POST['domain'].">
		
			<label>Your username:</label>
		
			<input type='text' class='input-text' name='username' size='30' value=".$_POST['username'].">
			
			<label>Your order number (enter n/a if not known):</label>
		
			<input type='text' class='input-text' name='ordernum' size='30' value=".$_POST['ordernum'].">
		
			<label>Your name:</label>
		
			<input type='text' class='input-text' name='fullname' size='30' value=".$_POST['fullname'].">
			
			<label>Your address:</label>
			<div class='ta-textarea1'><div class='ta-textarea2'><div class='ta-textarea3'><div class='ta-textarea4'><div class='ta-textarea5'><div class='ta-textarea6'><div class='ta-textarea7'><div class='ta-textarea8'>
			<textarea wrap='ON' name='address' cols='40' rows='5'>".$_POST['address']."</textarea>
			</div></div></div></div></div></div></div></div>
			<div class='clear'></div>
			
			<label>Your phone number:</label>
		
			<input type='text' class='input-text' name='phone' size='30' value=".$_POST['phone'].">
		
			<label>Your email address:</label>	
			
			<input type='text' class='input-text' name='email' size='30' value=".$_POST['email'].">
	
		</div>
		
		<div class='ca-col2'>
		
			<h4>What type of hosting plan did you have?</h4>
			
			<table cellpadding='0' cellspacing='0' class='ca-table ca-table2'>
			<tr>
				<td class='ca-table-radio'>
				<input type='radio' name='plan_type' value='Shared Hosting' checked='checked' /></td><td>Shared hosting</td>
			</tr>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='plan_type' value='Reseller Hosting' /></td><td>Reseller hosting</td>
			</tr>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='plan_type' value='Virtual Private Server' /></td><td>Virtual Private Server</td>
			</tr>
		</table>
			
			<h4>How long have you stayed with us?</h4>
			
			<table cellpadding='0' cellspacing='0' class='ca-table ca-table2'>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='duration' value='Less than one month' checked='checked' /></td><td>Less than one month</td>
			</tr>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='duration' value='Less than one year but more than one month' /></td><td>Less than one year but more than one month</td>
			</tr>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='duration' value='More than one year' /></td><td>More than one year</td>
			</tr>
		</table>
			
			<h4>Were you overall happy with WebHostingBuzz.com?</h4>
			
			<table cellpadding='0' cellspacing='0' class='ca-table ca-table2'>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='overall_happy' value='Yes' checked='checked' /></td><td>Yes</td>
			</tr>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='overall_happy' value='No' /></td><td>No</td>
			</tr>
		</table>
			
			<h4>Would you recommend WebHostingBuzz.com to a friend?</h4>
			
			<table cellpadding='0' cellspacing='0' class='ca-table ca-table2'>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='recommend' value='Yes' checked='checked' /></td><td>Yes</td>
			</tr>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='recommend' value='No' /></td><td>No</td>
			</tr>
		</table>
			
			<h4>Overall do you think you have received value for money?</h4>
			
			<table cellpadding='0' cellspacing='0' class='ca-table ca-table2'>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='value' value='Yes' checked='checked' /></td><td>Yes</td>
			</tr>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='value' value='No' /></td><td>No</td>
			</tr>
		</table>
	
	</div>
	<div class='clear'></div>
	
	<div class='ca-col1'>
		<h3>Customer Service:</h3>
			
		<h4>When you initially set up your hosting account, how was that experience?</h4>
		<table cellpadding='0' cellspacing='0' class='ca-table ca-table2'>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='init_setup' value='Excellent &ndash; Information was clear and it was a very easy process' checked='checked' /></td><td>Excellent &ndash; Information was clear and it was a very easy process</td>
			</tr>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='init_setup' value='It was okay &ndash; A little confusing but okay in the end.' /></td><td>It was okay &ndash; A little confusing but okay in the end.</td>
			</tr>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='init_setup' value='It was quite confusing &ndash; it took some time to get running.' /></td><td>It was quite confusing &ndash; it took some time to get running.</td>
			</tr>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='init_setup' value='It was a total nightmare.' /></td><td>It was a total nightmare.</td>
			</tr>
		</table>
			
		<h3>Performance / Reliability</h3>
		<h4>How reliable has your hosting account been with WebHostingBuzz.com?</h4>
		<table cellpadding='0' cellspacing='0' class='ca-table ca-table2'>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='reliability' value='Excellent &ndash; I have never noticed any downtime or problems &ndash; site is always fast.' checked='checked' /></td><td>Excellent &ndash; I have never noticed any downtime or problems &ndash; site is always fast.</td>
			</tr>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='reliability' value='Good &ndash; Some minor downtime communicated in advance, but the site has always been fast.'></td><td>Good &ndash; Some minor downtime communicated in advance, but the site has always been fast.</td>
			</tr>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='reliability' value='Okay &ndash; Site occasionally runs slow or is not available.' /></td><td>Okay &ndash; Site occasionally runs slow or is not available.</td>
			</tr>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='reliability' value='Poor &ndash; I have experienced constant problems with uptime and site speed.' /></td><td>Poor &ndash; I have experienced constant problems with uptime and site speed.</td>
			</tr>
		</table>
		
		<h3>Support:</h3>
		<h4>What is your experience dealing with our technical support department?</h4>
		<table cellpadding='0' cellspacing='0' class='ca-table ca-table2'>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='tech_exp' value='I have never had to deal with the technical support department.' checked='checked' /></td><td>I have never had to deal with the technical support department.</td>
			</tr>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='tech_exp' value='You were excellent &ndash; very responsive and helpful, always fixing problems easily.' /></td><td>You were excellent &ndash; very responsive and helpful, always fixing problems easily.</td>
			</tr>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='tech_exp' value='You were okay &ndash; You help with any problems but sometimes it takes a while.' /></td><td>You were okay &ndash; You help with any problems but sometimes it takes a while.</td>
			</tr>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='tech_exp' value='You were poor in this area &ndash; very unresponsive.' /></td><td>You were poor in this area &ndash; very unresponsive.</td>
			</tr>
		</table>
	
	</div>
	
	<div class='ca-col2'>
		<h3>Control Panel / Site Administration:</h3>
		<h4>How easy has it been to administer your site?</h4>
		<table cellpadding='0' cellspacing='0' class='ca-table ca-table2'>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='easy_admin' value='I have never used it or dont know if it exists.' checked='checked' /></td><td>I've never used it or don't know if it exists.</td>
			</tr>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='easy_admin' value='Very easy &ndash; I just did everything myself from your easy to understand control panel.' /></td><td>Very easy &ndash; I just did everything myself from your easy to understand control panel.</td>
			</tr>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='easy_admin' value='A little confusing but no problem after I figured it out.' /></td><td>A little confusing but no problem after I figured it out.</td>
			</tr>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='easy_admin' value='It is quite hard to use &ndash; I really dont like it.' /></td><td>It is quite hard to use &ndash; I really don't like it.</td>
			</tr>
		</table>
		
		<label>Please explain any additional information regarding your cancellation below, or any feedback you wish to provide.</label>
		
		<div class='ta-textarea1'><div class='ta-textarea2'><div class='ta-textarea3'><div class='ta-textarea4'><div class='ta-textarea5'><div class='ta-textarea6'><div class='ta-textarea7'><div class='ta-textarea8'>
			<textarea wrap='ON' name='reasons' cols='40' rows='5'>".$_POST['reasons']."</textarea>
		</div></div></div></div></div></div></div></div>
		<div class='clear'></div>
			
		<h4>When do you wish to cancel your account?</h4>
		<table cellpadding='0' cellspacing='0' class='ca-table ca-table2'>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='when_cancel' value='Immediately' checked='checked' /></td><td>Immediately (refund if under 45 days since the signup for shared and reseller hosting or 30 days for VPS hosting)</td>
			</tr>
			<tr>
				<td class='ca-table-radio'><input type='radio' name='when_cancel' value='End' /></td><td>At the end of the pre-paid period</td>
			</tr>
		</table>
		
	</div>
	<div class='clear'></div>
		
			<input type='hidden' name='action' value='cancel' />
			";
		
		
			echo "<div class='ca-btns'>
				<div class='back-box'>
					<input type='button'  OnClick='history.go(-1)' name='choicebt' value='Go Back' class='input-back' />
				</div> 
				<div class='submit-box'>
					<input type='submit' name='choicebt' value='Submit' class='input-submit' />
				</div>
			</div>	
			<div class='clear'></div>";  
			
		}
	}
	else
	{
		echo "<p><span class='bold'>Remember:</span> WebHostingBuzz will work with you personally to resolve any concerns you have with your webhosting plan. Why should you bounce from host to host?<br />You shouldn't, and that's why we differ from the other hosting companies out there. We care about our clients. Please consider discussing any account issues with management, and we will work hard to be sure that you are happy.<br /><a href='http://www.whbsupport.com'>Submit a ticket to our helpdesk</a>.</p>";
		
		echo "<p><span class='green'>Recommended Resolution:</span><br />";
		
		echo ${$temp};
		echo "</p>";
		echo "
		
		
		
		<input type='hidden' name='action' value='stillcancel' />
		";
		
		echo "<div class='ca-btns'>
			<div class='back-box'>
				<input type='button'  OnClick='history.go(-1)' name='choicebt' value='Go Back' class='input-back' />
			</div>
			<div class='submit-box'>
				<input type='submit' name='choicebt' value='I still want to cancel' class='input-submit' />
			</div>
		</div>
		<div class='clear'></div>";
	}
}

echo "</form>";
?>

<!-- End Cancellation form code -->