<?php

class ObjectACF
{

    private $containers = [];

    private $container = [];

    private $main_name = null;

    private $layouts = null;


    // testing
    private $tmp_path = [];

    private $level = null;

    public function __construct(array $array)
    {

        if(empty($array)){
            throw new Exception( 'Array is empty', 400);
        }

        $this->layouts = $array['sub_fields'][0]['layouts'];

        $this->loopLayouts();

    }

    private function setContainer($key, $value)
    {
        $this->container[$key] = $value;
    }

    private function getContainer($container, $key){
        if(!empty($this->containers[$container][$key])){
            return $this->containers[$container][$key];
        }else{
            return null;
        }
    }

    /**
     * The function use the $array and make structure the block of layout
     * @param $array
     */
    protected function compact($array, $pid = 0, $level = 0)
    {

        foreach ($array as $item){

            if($item['ID'] == $pid){
                if (isset($item['layouts'])){
                    return $this->compact($item['layouts']);
                }else if(isset($item['sub_fields'])){
                    return $this->compact($item['sub_fields']);
                }

//                $this->compact($array);
            }



        }

    }

    /**
     * The function sort out layouts and performs work on layouts
     * @return bool
     * @throws Exception
     */
    private function loopLayouts()
    {

        if(empty($this->layouts)){
            throw new Exception('Layouts is empty');
        }

        // compact all layouts in container
        foreach ($this->layouts as $layout){
            try{

                $this->tmp_path = $this->container = []; // clear container and tmp_path

                array_push($this->tmp_path, $layout['name']);

//                $this->container[$layout['name']] = []; // make key of container
//                $this->main_name = $layout['name'];
                $this->compact($layout['sub_fields'][0]['layouts']); // compact container

                array_push($this->containers, $this->container);

            }catch (Exception $exception){
                return false;
            }
        }

        return true;

    }



}