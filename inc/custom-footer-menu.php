<?php

/**
 * The function return top menu
 */
function get_footer_menu(){

    $menu_name = 'footer_menu';
    $locations = get_nav_menu_locations();

    if( $locations && isset( $locations[ $menu_name ] ) ){

        // получаем элементы меню
        $menu_items = wp_get_nav_menu_items( $locations[ $menu_name ]);

        return new ConstructorFooterMenu($menu_items);

    }

    return null;

}

class ConstructorFooterMenu
{
    private $top = '';

    private $sub_menu = '';

    /**
     * list items in the submenu
     * @var array
     */
    private $sub_items = [];

    private $menu_items = [];

    public function __construct($menu_items)
    {
        if(empty($menu_items)){
            return null;
        }

        $this->menu_items = $menu_items;

        $tree = [];

        // prepare tree
        foreach ($this->menu_items as $key => $item)
        {
            $tree[$item->ID] = $item;
            $tree[$item->ID]->children = [];
        }

        // make tree
        $this->menu_items = $this->createTree($tree);

//        $this->makeTopMenu();
//
        $this->makeSubMenu();
    }

    public function getTop()
    {
        return $this->top;
    }

    public function getSubMenu()
    {
        return $this->sub_menu;
    }

    public function addTopHtml($text)
    {
        $this->top .= $text;
    }

    public function addSubMenuHtml($text)
    {
        $this->sub_menu .= $text;
    }


    private function createTree(&$elements, $parent_id = 0)
    {
        $tree = array();

        foreach ($elements as $element){
            if ($element->menu_item_parent == $parent_id)
            {
                $children = $this->createTree($elements, $element->ID);
                if($children){
                    $element->children = $children;
                }
                $tree[$element->ID] = $element;
                unset($elements[$element->ID]);
            }
        }

        return $tree;
    }

    private function makeSubMenu()
    {

        $this->addSubMenuHtml('<div id="footer-bottom" class="row pt-4">');

            foreach ($this->menu_items as $key => $items)
            {

                $font = ' font-300';

                if(array_key_first($this->menu_items) == $key){
                    $font = ' font-600';
                }

                $this->addSubMenuHtml('<div class="col-md-3 col-12 mb-3">');
                    $this->addSubMenuHtml('<header class="footer__list__name color-green text-uppercase">
                        '. $items->title .'
                    </header>');
                        $this->addSubMenuHtml('<ul class="nav flex-column">');

                            foreach ($items->children as $child)
                            {

                                $class = !empty($child->children) ? ' dropdown ' : '';
                                $this->addSubMenuHtml('<li class="nav-item pl-0 '. $class . '">');


                                $class = empty($child->children) ? '' : ' dropdown-btn ';

                                $this->addSubMenuHtml('<a class="'.$class.' nav-link nav-link_sm pl-0 nav-link_white d-flex align-items-center position-relative '.$font.'" href="'. $child->url .'" aria-container=".sub-item-menu-'. $child->ID .'">');
                                $this->addSubMenuHtml( $child->title);
                                if(!empty($child->children)){
                                    $this->addSubMenuHtml('<span class="chevron-top-menu">
                                                                                            <svg    
                                                                                                    class="bi bi-chevron-right chevron-green"
                                                                                                    width="1em"
                                                                                                    height="1em" viewBox="0 0 16 16"
                                                                                                    fill="currentColor"
                                                                                                    xmlns="http://www.w3.org/2000/svg">
                                                                                              <path fill-rule="evenodd"
                                                                                                    d="M4.646 1.646a.5.5 0 01.708 0l6 6a.5.5 0 010 .708l-6 6a.5.5 0 01-.708-.708L10.293 8 4.646 2.354a.5.5 0 010-.708z"
                                                                                                    clip-rule="evenodd"/>
                                                                                            </svg>
                                                                                        </span>');
                                }
                                $this->addSubMenuHtml('</a>');

                                if(!empty($child->children)){
                                    $this->addSubMenuHtml('<div id="sub-item-menu-'. $child->ID .'" class="item-sub-menu sub-item-menu-'. $child->ID .'">');
                                    $this->addSubMenuHtml('<ul class="navbar-nav flex-sm-row flex-wrap">');

                                    foreach ($child->children as $sub_child)
                                    {
                                        $this->addSubMenuHtml('<li class="nav-item">');
                                        $this->addSubMenuHtml('<a class="nav-link color-white" href="'. $sub_child->url .'">');
                                        $this->addSubMenuHtml($sub_child->title);
                                        $this->addSubMenuHtml('</a>');
                                        $this->addSubMenuHtml('</li>');
                                    }

                                    $this->addSubMenuHtml('</ul>');
                                    $this->addSubMenuHtml('</div>');
                                }



                                $this->addSubMenuHtml('</li>');


                            }

                        $this->addSubMenuHtml('</ul>');
                $this->addSubMenuHtml('</div>');
            }

        $this->addSubMenuHtml('</section>');

    }



}
?>