<?php get_header(); ?>
<?php if (have_posts()) :?>
	<div class="content">
		<?php while (have_posts()) :?>
			<?php the_post();?>
			<h2><?php the_title(); ?></h2>
			<p><?php the_excerpt(); ?></p>
			<img class="span24" src="<?php echo esc_url( wp_get_attachment_url() ); ?>" alt="<?php the_title(); ?>">
		<?php endwhile;?>
	</div>
<?php endif; ?>
<?php get_footer(); ?>