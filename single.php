<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package jolt-v5
 */

get_header();
?>



<?php
get_sidebar();
get_footer();
