// todo: need to be completed for this task https://app.clickup.com/t/10datcw
function maxDigitFromString(string) {
    // let re = new RegExp("/\d/", "g")
    // let result = re.exec(string)
    console.log(string.match("/\d/g"))
}

function compressingProduct(list, colors = {low: "",mid: "",high: ""} ) {
    for (let item of list) {
        item[0]
    }
}

jQuery(document).ready(function ($) {

    $(".custom__section").each((k,i) => {
        let product_card_head_top = 0,
            product_card_head_name = 0,
            products =$(i).find('.product__card')

        // sort by max of height with padding
        products.each(function(k,i) {
            let t_h_t = $(i).find('.product__header__top').innerHeight()
            let t_h_n = $(i).find('.product__header__name').innerHeight()
            product_card_head_top = t_h_t > product_card_head_top ? t_h_t : product_card_head_top
            product_card_head_name =  t_h_n > product_card_head_name ? t_h_n : product_card_head_name
        })

        // set min height with value of max of products
        products.each(function(k,i) {
            let css = function(min) {
                return {
                    "display": "flex",
                    "flex-direction": "column",
                    "justify-content": "center",
                    "min-height": min + "px"
                }
            }
            if (product_card_head_top) {
                $(i).find('.product__header__top').css(css(product_card_head_top))
            }

            if (product_card_head_name) {
                $(i).find('.product__header__name').css(css(product_card_head_name))
            }
        })

    })

    $(".additional__open").click(function () {
       let prodcuts = $(this).parents(".product__card").parent().find(".product__card")
        prodcuts.toggleClass('features')
        console.log(prodcuts)
    });

})
