class AbstractDropdownMenu {
    debug = false
    config = null
    jquery = null
    nav_link = 'nav-link'
    dropdown_link = 'dropdown-btn'
    link_sub_menu = null
    container = null
    main_container = null
    exception_selectors = []
    scope = []

    constructor(jQ, mainContainer, linkSubMenu) {
        this.jquery = jQ
        this.main_container = this.jquery(mainContainer)
        this.link_sub_menu = linkSubMenu
    }

    /**
     *
     * @return {string}
     */
    scopeGet() {
        if (this.scope != '') {
            return this.scope.toString();
        } else {
            return []
        }
    }

    /**
     * The function add to
     * @param scopes
     */
    scopeSet(scopes) {
        if (this.jquery.isArray(scopes)) {
            scopes.filter(scope => {
                if (this.scope.indexOf(scope) === -1) {
                    this.scope.push(scope);
                }
            })
        }

        return this
    }

    static nameToClass(name) {
        return "." + name;
    }

    static nameToID(name) {
        return "#" + name;
    }

    setContainer(el) {
        if (el.attr('aria-container') !== '') {
            this.container = this.jquery(el.attr('aria-container'))
        }

        if (this.debug) {
            console.log('setContainer', el.attr('aria-container'))
        }

        return this
    }

    /**
     *
     * @return {jquery}
     */
    getContainer() {
        return this.container
    }

    disableContainer() {

        if (!this.getContainer()) {
            return this
        }

        if (this.debug) {
            console.log('disableContainer', this.getContainer())
        }

        this.getContainer().removeClass('open')

        return this
    }

    enableContainer() {
        if (!this.getContainer()) {
            return this
        }
        this.getContainer().addClass('open')
    }

    disableNavLinks() {
        if (!!(this.nav_link)) {
            this.jquery(this.scopeGet()).removeClass('active')
        }
    }

    enableNavLink(element) {
        if (!!(this.nav_link)) {
            this.disableNavLinks()
            element.addClass('active')
        }
    }

    disableSubMenu() {
        if (!!(this.link_sub_menu)) {
            this.jquery(DropdownMenu.nameToClass(this.link_sub_menu)).removeClass('open')
        }
    }

    enableSubMenu() {
        if (!!(this.link_sub_menu)) {
            this.disableSubMenu()
            this.jquery(DropdownMenu.nameToClass(this.link_sub_menu)).addClass('open')
        }
    }

    disableMainContainer() {
        if (!!(this.main_container)) {
            this.main_container.removeClass('open')
        }
    }

    enableMainContainer() {
        if (!!(this.main_container)) {
            this.disableMainContainer()
            this.main_container.addClass('open')
        }
    }


}

/**
 * Main container #sub-menu
 * jQuery = any version jquery
 */
class DropdownMenu extends AbstractDropdownMenu {

    submenu_timeout = 150
    exception_selectors = [
        '#sub-menu',
        '#top-panel',
        '#footer',
    ]

    __allow = null
    element = null

    exception_foreign = []

    last_element = []

    constructor(jQ, mainContainer, linkSubMenu) {
        super(jQ, mainContainer, linkSubMenu)
    }


    observe($) {
        let app = this

        $(app.scopeGet()).bind(app.getConfig())

        return app
    }

    observeIn(app, e, $) {
        let element = $(e.target)

        if (app.checkException(element)) {
            if (this.debug){
                console.log('observeIn', 'checkException')
            }
            return app.closeMenu()
        }

        if(app.checkForeign(element.attr('__permission'))){
            if (this.debug){
                console.log('observeIn', 'checkForeign')
            }
            app.closeMenu()
        }

        if (!(element instanceof $)) {
            return false
        }

        if (element.closest('a').attr('aria-container') == 0) {
            return app.closeMenu()
        }

        if (app.validate(element)) {
            e.preventDefault()
            return app.openMenu($, element.closest('a'))
        }

        return app
    }

    observeOut(app, e, $) {
        e.preventDefault()
        let element = $(e.target)


        if (app.checkException(element)) {
            return app.closeMenu()
        }

    }

    // todo: do the validation or do better
    validate(element) {
        let app = this

        if (!element.hasClass(app.dropdown_link) && !element.closest('a').hasClass(app.dropdown_link)) {
            if(app.debug){
                console.error('doesn`t have class', element)
            }
            return false
        }

        let container

        if(!element.attr('aria-container')){
            container = element.closest('a').attr('aria-container')
        }else{
            container = element.attr('aria-container')
        }

        if (typeof container == 'undefined' || container == '' || container == 0) {
            if(app.debug){
                console.error('aria-container is empty or undefined', element)
            }
            return false
        }

        return true
    }

    openMenu($, el) {
        let app = this

        if (app.last_element.length && app.last_element[0].attr('aria-container') === el.attr('aria-container')) {
            return app
        }

        let __allow = el.attr('__allow')
        this.__allow = !__allow ? null : __allow;

        new Promise(function (resolve, reject) {

            app.last_element[0] = el
            app.disableMainContainer()

            setTimeout(() => resolve(1), app.submenu_timeout);

        }).then(result => {

            app.disableSubMenu()

            app.disableNavLinks()
            app.enableNavLink(el)

            app.disableContainer()
            app.setContainer(el)
            app.enableMainContainer()
            app.enableContainer()
        });


        return app
    }

    closeMenu() {
        let app = this

        app.last_element = []
        app.__allow = null

        new Promise(function (resolve, reject) {
            app.disableNavLinks()
            app.disableMainContainer()

            setTimeout(() => resolve(1), app.submenu_timeout);
        }).then(result => {
            app.disableSubMenu()
            app.disableContainer()
        })
    }

    doubleClick(app, e, $) {
        e.preventDefault()
        this.closeMenu()
    }

    checkException(el) {
        let name = null

        // check class of element for could sub-menus to be close
        // for (name of this.exception_close){
        //     if(el.hasClass('nav-link') && !el.hasClass('dropdown-btn')){
        //         return true
        //     }
        // }

        // check where is element element now and if the element is in a white zone, it will return false
        for (name of this.exception_selectors) {
            if (el.closest(name).length) {
                return false
            }
        }

        return true
    }

    checkForeign(permission = null) {
        if(!this.__allow && !permission){
            if (this.debug){
                console.error('checkForeign', permission, this.__allow)
            }
            return false
        }

        if(permission === this.__allow){
            return false
        }
        if (this.debug){
            console.error('checkForeign', true)
        }
        return true
    }

    setConfig(hoverIn = true, hoverOut = false, clickDefault = false) {
        let app = this
        this.config = {}

        if (hoverIn) {
            this.config['mouseenter'] = function (e) {
                app.observeIn(app, e, $)
            }
        }

        if (hoverOut) {
            this.config['mouseleave'] = function (e) {
                app.observeOut(app, e, $)

            }
        }

        if (clickDefault) {
            this.config['click'] = function (e) {
                if ($(e.target).hasClass('active')){
                    return app.doubleClick(app, e, $)
                }

                return app.observeIn(app, e, $)
            }
        }
    }

    getConfig() {
        return this.config
    }
}


jQuery(document).ready( function () {
    /**
     * Top Menu
     * @type {DropdownMenu}
     */
    let top_menu = new DropdownMenu(jQuery, '#sub-menu', 'dropdown-sub-menu')
    top_menu.setConfig(true, false, true)
    top_menu.scopeSet([
            '#top-panel .nav-link',
            '#signal-close'
        ]
    ).observe(top_menu.jquery)

    /**
     * Sub menu, one from top menu
     * @type {DropdownMenu}
     */
    let sub_menu = new DropdownMenu(jQuery, null)
    sub_menu.setConfig(true, false, true)
    sub_menu.scopeSet([
        '#sub-menu .nav-link',
    ]).observe(sub_menu.jquery)

    /**
     * Footer Menu
     * @type {DropdownMenu}
     */
    let footer_menu = new DropdownMenu(jQuery, null)
    footer_menu.setConfig(true, false, true)
    footer_menu.scopeSet([
        '#footer .nav-link'
    ]).observe(footer_menu.jquery)
})