let delete_hook_submenu = null

jQuery(document).ready(function ($) {
    $(".faq__item__header").click(function () {

        let all_body = $('.faq__item__body')

        let answer = $($(this).attr('data-target'))


        all_body.each((index, item) => {
            if(answer.attr('id') === $(item).attr('id')){
                $(item).toggleClass('show')
            }else{
                $(item).removeClass('show')
            }
        })

    })

    $('.reason__item').click(function (e) {
        $('.reason__item').removeClass('active')
        $(this).addClass('active')

        let key = $(this).attr('data-key')

        let wrap_content = $('.reason__content')

        wrap_content.css({
            'opacity': '0',
            'max-height': '0%',
            'min-height': '0%'
        })

        setTimeout(function () {
            $('.reason__content .article__block').removeClass('active')

            let article = $('#'+key)

            article.addClass('active')

            let max_height = article.outerHeight()


            wrap_content.css({
                'min-height': max_height,
                'max-height': max_height * 1.9999,
                'opacity': '1'
            })
        }, 100)
    })

    $('.reason__content .left').click(function (e) {
        let active = $('.reason__item.active')
        let item = ReasonFindItem(active, 'prev')
        ReasonChangeContent(item)
    })

    $('.reason__content .right').click(function (e) {
        let active = $('.reason__item.active')
        let item = ReasonFindItem(active, 'next')
        ReasonChangeContent(item)
    })

    let ReasonFindItem = function (active, func) {

        let f = eval('active.'+func+'()')

        console.log(f)

        if(f.hasClass('left') || (func === 'prev' && !f.length)){
            return $('.reason__item:last-child')
        }else if (f.hasClass('right') || (func === 'next' && !f.length)){
            return $('.reason__item:first-child')
        }
        return f

    }

    function ReasonChangeContent(item) {
        $('.reason__item').removeClass('active')
        item.addClass('active')
        let  key = item.attr('data-key')

        $('.reason__content .article__block').removeClass('active')
        $('#'+key).addClass('active')
    }

})