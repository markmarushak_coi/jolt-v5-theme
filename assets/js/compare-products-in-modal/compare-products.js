/**
 * @output themes/jolt-v5/assets/js/review-slider/main.js
 */

(function ($) {

    // register function reviewSliderItem
    jQuery.fn.reviewSliderItem = function () {
        let item = jQuery(this)

        let timeout = 1000

        item.status = 'hide'

        item.listStatus = ['prev', 'current', 'next']

        item.changeStatus = function (status) {
            item.status = status
            item.addClass( item.status )
            return item
        }

        item.getStatus = function () {
            return item.status
        }

        item.prevStep = function (slider) {
            item.listStatus.filter(status => {
                slider.listShow.minus(status)
            })
            slider.showItems(timeout)
        }

        item.nextStep = function (slider) {
            item.listStatus.filter(status => {
                slider.listShow.plus(status)
            })
            slider.showItems( timeout )
        }

        return item
    }

    // register function reviewSlider
    jQuery.fn.reviewSlider = function () {

        let slider = jQuery(this)

        slider.list = []

        slider.listShow = {
            prev: {
                val: 0,
                last: null
            },
            current: {
                val: 1,
                last: null
            },
            next: {
                val: 2,
                last: null
            },
            plus: function (key){
                let app = this,
                    item = this[key]

                item.last = item.val // save last value

                slider.list[item.val].addClass('to-left')

                if(item.val + 1 > slider.list.length - 1) {
                    item.val = 0
                }else{
                    item.val++
                }


                if(key === 'next'){
                    setTimeout(function () {
                        slider.list[app['prev'].last].addClass('hide')
                        slider.list[app['next'].val].addClass('hide-to-next-show')
                    }, 200)
                }

            },
            minus: function (key){
                let app = this,
                    item = this[key]

                item.last = item.val // save last value

                slider.list[item.val].addClass('to-right')

                if(item.val - 1 < 0) {
                    item.val = slider.list.length - 1
                }else{
                    item.val--
                }

                if(key === 'prev'){
                    setTimeout(function () {
                        slider.list[app['next'].last].addClass('hide')
                        slider.list[app['prev'].val].addClass('hide-to-prev-show')
                    }, 200)
                }
            }
        }

        slider.generateStr = function () {
            return Math.random().toString(36).substring(2)
        }

        slider.controls = {
            id: '',
            init: function (control) {
                this.id = control

                let left_id  = this.left.id = slider.generateStr(),
                    right_id = this.right.id = slider.generateStr()

                control.find('.control__left').attr('id', left_id)
                control.find('.control__right').attr('id', right_id)

                jQuery('#' + left_id).click(this.left.control)
                jQuery('#' + right_id).click(this.right.control)
            },
            current: function () {
                return slider.list.filter((item, index) => {
                    return item.getStatus() === 'current'
                }).pop()
            },
            left: {
                id: null,
                control: function () {
                    slider.controls.disable()
                    slider.controls.current().prevStep(slider)
                }
            },
            right: {
                id: null,
                control: function () {
                    slider.controls.disable()
                    slider.controls.current().nextStep(slider)
                }
            },
            disable: function () {
                this.id.addClass('disable')
            },
            enable: function () {
                this.id.removeClass('disable')
            }
        }

        slider.showItems = function (timeout = 0){
            if(timeout > 0) {
                setTimeout(slider.showItems, timeout)
                return 0
            }

            for (let item of slider.list){
                item[0].classList = ['slider__item']
            }

            slider.list[ slider.listShow[ 'prev' ].val ].changeStatus('prev')
            slider.list[ slider.listShow['current'].val ].changeStatus('current')
            slider.list[ slider.listShow['next'].val ].changeStatus('next')

            slider.controls.enable()
        }

        for (let item of slider.find('.slider__item')) {
            slider.list.push( jQuery(item).reviewSliderItem() )
        }

        slider.controls.init( slider.find('.slider__control') )
        slider.showItems()

    }
} ( jQuery ) );


function init_compare_products(products) {

    if (selectors.length === 0) {
        return 0;
    }

    if (typeof selectors !== 'object') {
        return console.error('init_review_slider haven`t got array. check data')
    }

    for (let slider of selectors) {
        jQuery(slider).reviewSlider()
    }

}

jQuery(document).ready(function () {
    init_review_slider(jQuery('.review__slider'))
})