<?php

ini_set("max_execution_time", -1);

$page_ids = get_all_page_ids();

foreach ($page_ids as $page_id) {

    //Store the micro time so that we know
    //when our script started to run.
    $time_start = microtime(true);

    $sections = get_field('sections', $page_id);

    $page = md5($page_id) . md5(json_encode($sections));

    $page_exists = \inc\cache\Cache::instance()->Find($page);

    if ($page_exists) {
        \inc\cache\Cache::instance()->Read($page);
    }else if ($sections) {
        $cache = "";
        foreach ($sections as $section) {
            $section = new \inc\builder\section\SectionFactory(
                $section['section'],
                new \inc\builder\section\Section('section')
            );

            $cache .= $section->render();
        }
        \inc\cache\Cache::instance()->Store($page, $cache);
    }


    //At the end of your code, compare the current
    //microtime to the microtime that we stored
    //at the beginning of the script.
    $time_end = microtime(true);

    //The result will be in seconds and milliseconds.
    $seconds = $time_end - $time_start;

    $page_time = [
        "time-duration" => $seconds,
        "title" => get_the_title($page_id),
        "page-id" => $page_id
    ];

    echo "<pre>" . var_dump($page_time) . "</pre>";
}

?>




