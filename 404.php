<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package jolt-v5
 */

get_header();
?>
    <section id="header" class="custom__section">
        <div class="background__header_white background background__header header__background"></div>
        <div class="container">
            <div class="row">
                <div id="" class="col-md-7 col">
                    <div class="row"><h1 class="header__title  col-12"><span
                                    class="header__title__name header__title__name-blue">Page</span><span
                                    class="header__title__sub ">Not Found</span></h1>
                        <div id="" class="offset-1 col">
                            <div class="row"><h2 class="header__description col-12 header__description-dark-blue">Looks
                                    like someone, somewhere has got their wires crossed.
                                    Sorry, this page has either moved, renamed or has been deleted. Please choose from
                                    one of the following options to continue using the website.</h2>
                                <div class="header__buttons col-12">
                                    <div class="buttons__list"><a href="/"
                                                                  class="buttons__item btn-green">homapage</a><a
                                                href="/support" class="buttons__item btn-outline-green-2">contact us</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header__picture">
                    <img class="img-100x100" src="/wp-content/themes/jolt-v5/assets/img/Group 35721.png"
                         alt="Page">
                </div>
            </div>
        </div>
    </section>
    <section class="custom_section custom__padding_top"></section>

<?php
get_footer();
