<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package jolt-v5
 */


$sections = get_field('sections', $post->ID);

$page = md5($post->ID) . md5(json_encode($sections));

$page_exists = \inc\cache\Cache::instance()->Find($page);

if ($page_exists) {
   echo \inc\cache\Cache::instance()->Read($page);
}else if ($sections) {
    $cache = "";
    foreach ($sections as $section) {
        $section = new \inc\builder\section\SectionFactory(
            $section['section'],
            new \inc\builder\section\Section('section')
        );

        $content = $section->render();

        $cache .= $content;

        echo $content;

    }
    \inc\cache\Cache::instance()->Store($page, $cache);
}

//the_content();