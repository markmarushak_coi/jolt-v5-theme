<?php
/*
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package jolt-v5
 */

$lazy_load = defined('TYPE_LOAD_PAGE') ? TYPE_LOAD_PAGE : false;

get_header();
?>
    <?php
        // DEBUG

        if (isset($_GET["tests"])) {
            $folder = $_GET["tests"];
            $file = $_GET["file"] ?? null;

            get_template_part("tests/$folder/test", $file);
            exit;
        }

    ?>

    <?php if ($lazy_load):?>
        <script async>
            let post_id = "<?php the_ID() ?>"
            let url = window.location.origin
            url += "/wp-json/jolt/builder/start/" + post_id

            fetch(url).then(async response => {
                let result = await response.json()
                let output = ''
                for (let section of result){
                    output += section
                }
                jQuery('#header').after(output)
            })


        </script>
    <?php else :?>
        <?php get_template_part('template-parts/content', 'page') ?>
    <?php endif;?>


<?php
get_footer();
