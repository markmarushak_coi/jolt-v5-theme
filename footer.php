<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package jolt-v5
 */

?>

<footer id="footer" class="bg-dark-blue footer">

    <div class="container">
        <div id="footer-top" class="row align-items-center justify-content-center custom__line_bottom-green pb-5">

            <div class="col-sm-2 col-4">
                <?php jolt_v5_the_custom_logo(); ?>
            </div>

            <div class="col-sm-10 col-8 p-0">

                <div class="d-flex flex-column align-items-end font-300">

                    <ul class="navbar-nav flex-row justify-content-end nav-item_border-right customer-menu"
                        id="customer-panel">
                        <li class="nav-item">
                            <a class="nav-link color-white nav-link_sm" href="javascript:void(0);"
                               onclick="olark('api.box.expand')">Live Chat</a>
                        </li>
                        <li class="nav-item-separate"></li>
                        <li class="nav-item">
                            <a class="nav-link color-white nav-link_sm" href="https://my.jolt.co.uk/register.php">Sign
                                Up</a>
                        </li>
                        <li class="nav-item-separate"></li>
                        <li class="nav-item">
                            <a class="nav-link color-white nav-link_sm" href="https://my.jolt.co.uk/clientarea.php">Log
                                In</a>
                        </li>
                    </ul>

                </div>

            </div>

        </div>


        <?php

        $footer_menu = get_footer_menu();

        if (!empty($footer_menu)) {
            echo $footer_menu->getSubMenu();
        }

        ?>


    </div>

</footer>

<section class="company-licence">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-12">
                © 2007 to <?= date('Y'); ?> Jolt. Formerly WebHostingBuzz UK. Registered in England and Wales, Company No. 06111631, VAT
                No. GB 994 2903 80. Host Lincoln Ltd T/A Jolt.co.uk, Halifax House, 30-34 George Street, Hull, HU1
                3AJ
            </div>
            <div class="col-lg-4 px-lg-0">
                <img loading="lazy" alt="Visa" src="<?php echo get_template_directory_uri()?>/assets/img/visa@2x.png"  height="30">
                <img loading="lazy" alt="MasterCard" src="<?php echo get_template_directory_uri()?>/assets/img/mastercard@2x.png"  height="30">
                <img loading="lazy" alt="American Express" src="<?php echo get_template_directory_uri()?>/assets/img/amex@2x.png"  height="30">
                <img loading="lazy" alt="PayPal" src="<?php echo get_template_directory_uri()?>/assets/img/paypal@2x.png"  height="30">
                <img loading="lazy" alt="Direct Debit" src="<?php echo get_template_directory_uri()?>/assets/img/direct_debit@2x.png"  height="30">
                <img loading="lazy" alt="Webmoney" src="<?php echo get_template_directory_uri()?>/assets/img/bitpay@2x.png"  height="30">
            </div>
        </div>
    </div>
</section>

</div><!-- #page -->


<?php if(get_field('has_schema')): ?>

    <script type="application/ld+json">

		{

			"@context": "http://schema.org",

			"@type": "Product",

			<?php if(get_field('schema_brand', 'option')): ?>

				"brand": "<?php the_field('schema_brand', 'option'); ?>",

			<?php endif; ?>

			<?php if(get_field('schema_productName')): ?>

				"name": "<?php the_field('schema_productName'); ?>",

			<?php endif; ?>

			<?php if(get_field('schema_description')): ?>

				"description": "<?php the_field('schema_description'); ?>",

			<?php endif; ?>

			<?php if(get_field('schema_image')): ?>

				"image": "<?php echo esc_url(get_field('schema_image')); ?>",

			<?php endif; ?>

			<?php if(get_field('schema_productID')): ?>

				"productID": "<?php the_field('schema_productID'); ?>",

			<?php endif; ?>

			"aggregateRating": {

			"@type": "AggregateRating",

			<?php if(get_field('schema_bestRating', 'option')): ?>

				"bestRating": "<?php the_field('schema_bestRating', 'option'); ?>",

			<?php endif; ?>

			<?php if(get_field('schema_ratingValue', 'option')): ?>

				"ratingValue": "<?php the_field('schema_ratingValue', 'option'); ?>",

			<?php endif; ?>

			<?php if(get_field('schema_reviewCount', 'option')): ?>

				"reviewCount": "<?php the_field('schema_reviewCount', 'option'); ?>"

			<?php endif; ?>

		},

		"offers": {

		"@type": "Offer",

		"url": "<?php the_permalink(); ?>",

		<?php if(get_field('schema_priceCurrency', 'option')): ?>

			"priceCurrency": "<?php the_field('schema_priceCurrency', 'option'); ?>",

		<?php endif; ?>

		<?php if(get_field('schema_price')): ?>

			"price": "<?php the_field('schema_price'); ?>",

		<?php endif; ?>

		<?php if(get_field('schema_priceValidUntil')): ?>

			"priceValidUntil": "<?php the_field('schema_priceValidUntil'); ?>",

		<?php endif; ?>

		<?php if(get_field('schema_itemCondition', 'option')): ?>

			"itemCondition": "<?php echo esc_url(get_field('schema_itemCondition', 'option')); ?>",

		<?php endif; ?>

		<?php if(get_field('schema_availability', 'option')): ?>

			"availability": "<?php echo esc_url(get_field('schema_availability', 'option')); ?>",

		<?php endif; ?>

		<?php if(get_field('schema_sellerName', 'option')): ?>

			"seller": {

			"@type": "Organization",

			"name": "<?php the_field('schema_sellerName', 'option'); ?>"

		}

	<?php endif; ?>

}

}

</script>

<?php endif; ?>


<script src="https://www.dwin1.com/23811.js"></script>

<?php if (get_field('footer_scripts', 'option')) : the_field('footer_scripts', 'option'); endif; ?>

<?php wp_footer(); ?>
</body>
</html>