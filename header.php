<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package jolt-v5
 */

?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 <?php language_attributes(); ?>"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8 <?php language_attributes(); ?>"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9 <?php language_attributes(); ?>"> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<html <?php language_attributes(); ?> <?php jolt_v5_schema_page(); ?>>
<head>
    <script type="text/javascript">
        function addScript(filename) {
            var head = document.getElementsByTagName('head')[0];

            var script = document.createElement('script');
            script.src = filename;
            script.type = 'text/javascript';

            head.insertBefore(script, document.getElementsByTagName("script")[0]);
        }
    </script>
    <script type="text/javascript">
        function loadCSS(filename) {
            var head = document.getElementsByTagName('head')[0];

            var style = document.createElement('link');
            style.href = filename;
            style.type = 'text/css';
            style.rel = 'stylesheet';
            head.appendChild(style);
        }
    </script>

    <!--
        Global site tag (gtag.js) - Google Analytics
        The google analytics tracking code should be just after the head for good prefomance
    -->
<!--    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-72028346-1"></script>-->
    <script>
        addScript("https://www.googletagmanager.com/gtag/js?id=UA-72028346-1")
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-72028346-1');
    </script>

    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- <link rel="profile" href="http://gmpg.org/xfn/11"> -->

    <!--[if lt IE 9]>
    <script async src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
    <script async src="<?php echo get_template_directory_uri(); ?>/js/selectivizr-min.js"></script>
    <![endif]-->
    <script async src="<?php echo get_template_directory_uri(); ?>/assets/js/static/jquery-3.4.1.slim.min.js"></script>
    <script async src="<?php echo get_template_directory_uri(); ?>/assets/js/static/popper.min.js"></script>
    <script async src="<?php echo get_template_directory_uri(); ?>/assets/js/static/bootstrap.min.js"></script>

    <?php wp_head(); ?>

    <!--  Tracking Pixel  -->
    <script src="https://www.dwin1.com/23811.js" type="text/javascript" defer="defer"></script>

    <!-- Facebook Pixel Code -->
    <script async>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version="2.0";
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,"script",
        "https://connect.facebook.net/en_US/fbevents.js");
            fbq("init", "303828576617273");
        fbq("track", "PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none" loading="lazy"
                   src="https://www.facebook.com/tr?id=303828576617273&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
</head>

<?php require_once 'inc/mobile-detect.php';?>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div class="wrapper">

    <?php
    $top_menu = get_top_menu();

    if (!empty($top_menu)) {
        echo $top_menu->getTop();

        echo $top_menu->getSubMenu();
    }


    $data = get_field('headers');

    if(!empty($data)){
        $header = new \inc\builder\header\HeaderFactory($data, new \inc\builder\header\Header('header'));

        if(!empty($header)){
            $header->render();
        }
    }

    ?>

